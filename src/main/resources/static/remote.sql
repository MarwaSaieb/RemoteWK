-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 23 mai 2023 à 11:58
-- Version du serveur : 10.4.27-MariaDB
-- Version de PHP : 8.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `remote`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `inserted_at` datetime DEFAULT NULL,
  `is_enabled` bit(1) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`id`, `date_of_birth`, `first_name`, `inserted_at`, `is_enabled`, `last_name`, `phone_number`, `role_id`, `user_id`) VALUES
(1, '2023-02-02', 'super', '2023-02-02 17:16:52', b'0', 'admin', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `confirmation_token`
--

CREATE TABLE `confirmation_token` (
  `tokenid` bigint(20) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `confirmation_token`
--

INSERT INTO `confirmation_token` (`tokenid`, `created_date`, `token`, `user_id`) VALUES
(1, '2023-02-02 17:16:52', '09148c63-9a0d-4496-9300-386cfa6e7058', 1),
(2, '2023-02-02 17:16:52', '98bd9344-d69a-49b0-a0b1-b67a50cd965d', 2),
(3, '2023-02-02 17:16:52', 'ca069fa0-c3d2-4b31-8258-22d7f5185a8b', 3),
(4, '2023-02-03 18:52:08', '0d937918-d83e-4560-8b27-f57755bf2edc', 4),
(7, '2023-02-03 20:40:32', '2d1691db-7273-431a-8789-2b688d3a74e0', 11),
(8, '2023-02-03 20:56:45', 'bda9cb71-1b2b-4da4-8726-c9a5a7358181', 12),
(9, '2023-05-08 14:58:54', 'c518f1f5-41dd-4dee-a05a-6f85eb9b467e', 13);

-- --------------------------------------------------------

--
-- Structure de la table `departement`
--

CREATE TABLE `departement` (
  `id` bigint(20) NOT NULL,
  `departement_name` varchar(255) DEFAULT NULL,
  `nbr_worker` bigint(20) DEFAULT NULL,
  `manager_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `departement`
--

INSERT INTO `departement` (`id`, `departement_name`, `nbr_worker`, `manager_id`) VALUES
(1, 'DIGITAL', 250, 1);

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

CREATE TABLE `evenement` (
  `id` bigint(20) NOT NULL,
  `date_creation` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `description` text DEFAULT NULL,
  `event_name` varchar(255) DEFAULT NULL,
  `type_event` bit(1) NOT NULL,
  `hrr_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `evenement`
--

INSERT INTO `evenement` (`id`, `date_creation`, `date_end`, `date_start`, `description`, `event_name`, `type_event`, `hrr_id`) VALUES
(1, '2023-05-08 16:06:36', '2023-05-08 19:00:00', '2023-05-08 16:06:36', 'team building', 'team building', b'1', 1),
(2, '2023-05-08 00:00:00', '2023-05-23 00:00:00', '2023-05-20 17:40:00', 'cscsc', 'scc', b'1', 1),
(4, '2023-05-09 00:00:00', '2023-05-20 00:00:00', '2023-05-16 00:00:00', 'update', 'demo update2', b'1', 1);

-- --------------------------------------------------------

--
-- Structure de la table `evenement_worker`
--

CREATE TABLE `evenement_worker` (
  `events_id` bigint(20) NOT NULL,
  `worker_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `evenement_worker`
--

INSERT INTO `evenement_worker` (`events_id`, `worker_id`) VALUES
(4, 6);

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(10);

-- --------------------------------------------------------

--
-- Structure de la table `hrr`
--

CREATE TABLE `hrr` (
  `id` bigint(20) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `inserted_at` datetime DEFAULT NULL,
  `is_enabled` bit(1) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `hrr`
--

INSERT INTO `hrr` (`id`, `date_of_birth`, `first_name`, `inserted_at`, `is_enabled`, `last_name`, `phone_number`, `role_id`, `user_id`) VALUES
(1, NULL, NULL, NULL, b'0', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `it`
--

CREATE TABLE `it` (
  `id` bigint(20) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `inserted_at` datetime DEFAULT NULL,
  `is_enabled` bit(1) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `it`
--

INSERT INTO `it` (`id`, `date_of_birth`, `first_name`, `inserted_at`, `is_enabled`, `last_name`, `phone_number`, `role_id`, `user_id`) VALUES
(1, '2023-02-02', 'It', '2023-02-02 17:16:52', b'0', 'IT', NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Structure de la table `manager`
--

CREATE TABLE `manager` (
  `id` bigint(20) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `inserted_at` datetime DEFAULT NULL,
  `is_enabled` bit(1) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `manager`
--

INSERT INTO `manager` (`id`, `date_of_birth`, `first_name`, `inserted_at`, `is_enabled`, `last_name`, `phone_number`, `role_id`, `user_id`) VALUES
(1, '2023-02-15', 'mang', '2023-02-15 21:17:26', b'0', 'mang', NULL, 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `material`
--

CREATE TABLE `material` (
  `id` bigint(20) NOT NULL,
  `etat` bit(1) NOT NULL,
  `material_type` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `request_remote_id` bigint(20) DEFAULT NULL,
  `worker_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

CREATE TABLE `pays` (
  `id` bigint(20) NOT NULL,
  `pays_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `planning`
--

CREATE TABLE `planning` (
  `id` bigint(20) NOT NULL,
  `date_creation` datetime DEFAULT NULL,
  `date_remote` datetime DEFAULT NULL,
  `day` varchar(255) DEFAULT NULL,
  `month` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `years` varchar(255) DEFAULT NULL,
  `request_remote_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `planning`
--

INSERT INTO `planning` (`id`, `date_creation`, `date_remote`, `day`, `month`, `status`, `years`, `request_remote_id`) VALUES
(1, '2023-02-07 20:38:58', '2023-05-18 00:00:00', '7', '1', 'Remote', '2023', 1),
(2, '2023-02-07 20:38:58', '2023-02-08 00:00:00', '8', '1', 'Remote', '2023', 1),
(3, '2023-02-07 20:38:58', '2023-02-09 00:00:00', '9', '1', 'Remote', '2023', 1),
(4, '2023-02-07 20:38:58', '2023-02-10 00:00:00', '10', '1', 'Remote', '2023', 1),
(5, '2023-02-07 20:38:58', '2023-02-13 00:00:00', '13', '1', 'Remote', '2023', 1),
(6, '2023-02-07 20:38:58', '2023-02-14 00:00:00', '14', '1', 'Remote', '2023', 1),
(7, '2023-02-07 20:38:58', '2023-02-15 00:00:00', '15', '1', 'Remote', '2023', 1),
(8, '2023-02-07 20:38:58', '2023-02-16 00:00:00', '16', '1', 'Remote', '2023', 1),
(9, '2023-02-07 20:38:58', '2023-02-17 00:00:00', '17', '1', 'Remote', '2023', 1),
(10, '2023-02-07 20:38:58', '2023-02-20 00:00:00', '20', '1', 'Remote', '2023', 1),
(11, '2023-02-07 20:38:58', '2023-02-21 00:00:00', '21', '1', 'Remote', '2023', 1),
(12, '2023-02-07 20:38:58', '2023-02-22 00:00:00', '22', '1', 'Remote', '2023', 1),
(13, '2023-02-07 20:38:58', '2023-02-23 00:00:00', '23', '1', 'Remote', '2023', 1),
(14, '2023-02-07 20:38:58', '2023-02-24 00:00:00', '24', '1', 'Remote', '2023', 1),
(15, '2023-02-07 20:38:58', '2023-02-27 00:00:00', '27', '1', 'Remote', '2023', 1),
(16, '2023-02-07 20:38:58', '2023-02-28 00:00:00', '28', '1', 'Remote', '2023', 1),
(17, '2023-02-07 20:41:48', '2023-02-13 00:00:00', '13', '1', 'Remote', '2023', 2),
(18, '2023-02-07 20:41:48', '2023-02-14 00:00:00', '14', '1', 'Remote', '2023', 2),
(19, '2023-02-07 20:41:48', '2023-02-15 00:00:00', '15', '1', 'Remote', '2023', 2),
(20, '2023-02-07 20:41:48', '2023-02-16 00:00:00', '16', '1', 'Remote', '2023', 2),
(21, '2023-02-07 20:41:48', '2023-02-17 00:00:00', '17', '1', 'Remote', '2023', 2),
(22, '2023-02-07 20:41:48', '2023-02-20 00:00:00', '20', '1', 'Remote', '2023', 2),
(23, '2023-02-07 20:41:48', '2023-02-21 00:00:00', '21', '1', 'Remote', '2023', 2),
(100, '2023-03-24 13:22:23', '2023-05-04 00:00:00', '4', '1', 'Remote', '2023', 3),
(101, '2023-03-24 13:22:23', '2023-05-05 00:00:00', '5', '1', 'Remote', '2023', 3),
(102, '2023-03-24 13:22:23', '2023-05-08 00:00:00', '8', '1', 'Remote', '2023', 3),
(103, '2023-03-24 13:22:23', '2023-05-09 00:00:00', '9', '1', 'Remote', '2023', 3),
(104, '2023-03-24 13:22:23', '2023-05-10 00:00:00', '10', '1', 'Remote', '2023', 3),
(105, '2023-03-24 13:22:23', '2023-05-11 00:00:00', '11', '1', 'Remote', '2023', 3),
(106, '2023-03-24 13:22:23', '2023-05-12 00:00:00', '12', '1', 'Remote', '2023', 3),
(107, '2023-03-24 13:22:23', '2023-05-15 00:00:00', '15', '1', 'Remote', '2023', 3),
(108, '2023-05-04 13:34:57', '2023-05-22 00:00:00', '22', '1', 'Remote', '2023', 4),
(109, '2023-05-04 13:34:57', '2023-05-23 00:00:00', '23', '1', 'Remote', '2023', 4),
(110, '2023-05-04 13:34:57', '2023-05-24 00:00:00', '24', '1', 'Remote', '2023', 4),
(111, '2023-05-04 13:34:57', '2023-05-25 00:00:00', '25', '1', 'Remote', '2023', 4),
(112, '2023-05-04 13:34:57', '2023-05-26 00:00:00', '26', '1', 'Remote', '2023', 4),
(113, '2023-05-04 13:34:57', '2023-05-29 00:00:00', '29', '1', 'Remote', '2023', 4),
(114, '2023-05-04 13:34:57', '2023-05-30 00:00:00', '30', '1', 'Remote', '2023', 4),
(115, '2023-05-04 13:34:57', '2023-05-31 00:00:00', '31', '1', 'Remote', '2023', 4);

-- --------------------------------------------------------

--
-- Structure de la table `project`
--

CREATE TABLE `project` (
  `id` bigint(20) NOT NULL,
  `project_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `project`
--

INSERT INTO `project` (`id`, `project_name`) VALUES
(1, 'Project1');

-- --------------------------------------------------------

--
-- Structure de la table `region`
--

CREATE TABLE `region` (
  `id` bigint(20) NOT NULL,
  `region_name` varchar(255) DEFAULT NULL,
  `pays_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `request_document`
--

CREATE TABLE `request_document` (
  `id` bigint(20) NOT NULL,
  `status` bit(1) NOT NULL,
  `text` text DEFAULT NULL,
  `typedoc` varchar(255) DEFAULT NULL,
  `manager_id` bigint(20) DEFAULT NULL,
  `worker_id` bigint(20) DEFAULT NULL,
  `date_creation` datetime DEFAULT NULL,
  `date_document` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `request_document`
--

INSERT INTO `request_document` (`id`, `status`, `text`, `typedoc`, `manager_id`, `worker_id`, `date_creation`, `date_document`) VALUES
(1, b'0', 'ss', 'Contract', NULL, 1, '2023-05-11 00:00:00', '2023-05-24 00:00:00'),
(2, b'1', 'tttttttttttttt', 'Certificate', NULL, 6, '2023-05-08 00:00:00', '2023-05-26 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `request_remote`
--

CREATE TABLE `request_remote` (
  `id` bigint(20) NOT NULL,
  `confirme_it` bit(1) NOT NULL,
  `confirme_manager` bit(1) NOT NULL,
  `date_end` datetime DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `description` text DEFAULT NULL,
  `expired` bit(1) NOT NULL,
  `nbr_day` bigint(20) DEFAULT NULL,
  `it_id` bigint(20) DEFAULT NULL,
  `manager_id` bigint(20) DEFAULT NULL,
  `worker_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `request_remote`
--

INSERT INTO `request_remote` (`id`, `confirme_it`, `confirme_manager`, `date_end`, `date_start`, `description`, `expired`, `nbr_day`, `it_id`, `manager_id`, `worker_id`) VALUES
(1, b'1', b'1', '2023-02-28 00:00:00', '2023-02-07 00:00:00', '', b'0', 16, 1, 1, 1),
(2, b'1', b'1', '2023-02-21 00:00:00', '2023-02-13 00:00:00', '', b'0', 7, 1, 1, 8),
(3, b'0', b'0', '2023-05-15 00:00:00', '2023-05-04 00:00:00', 'test demo 2', b'0', 8, NULL, NULL, 1),
(4, b'0', b'0', '2023-05-31 00:00:00', '2023-05-22 00:00:00', '', b'0', 8, NULL, NULL, 6);

-- --------------------------------------------------------

--
-- Structure de la table `risques`
--

CREATE TABLE `risques` (
  `id` bigint(20) NOT NULL,
  `date_of_risque` datetime DEFAULT NULL,
  `description` text DEFAULT NULL,
  `status` bit(1) NOT NULL,
  `type_risque` varchar(255) DEFAULT NULL,
  `worker_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE `role` (
  `id` bigint(20) NOT NULL,
  `role` enum('ROLE_ADMIN','ROLE_MANAGER','ROLE_HRR','ROLE_IT','ROLE_WORKER') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'ROLE_ADMIN'),
(2, 'ROLE_MANAGER'),
(3, 'ROLE_HRR'),
(4, 'ROLE_IT'),
(5, 'ROLE_WORKER');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `inserted_at` datetime DEFAULT NULL,
  `is_enabled` bit(1) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `inserted_at`, `is_enabled`, `password`, `phone_number`, `username`) VALUES
(1, 'admin@capgemini.com', '2023-02-02 17:16:52', b'1', '$2a$10$KFkINm4oioSB8E3qygik4uA4Nq6gwZawo7OFz2mfAQabPGkZiUv3i', '11111111', 'superadmin'),
(2, 'manager@capgemini.com', '2023-02-02 17:16:52', b'1', '$2a$10$KFkINm4oioSB8E3qygik4uA4Nq6gwZawo7OFz2mfAQabPGkZiUv3i', '22222222', 'manager'),
(3, 'it@capgemini.com', '2023-02-02 17:16:52', b'1', '$2a$10$KFkINm4oioSB8E3qygik4uA4Nq6gwZawo7OFz2mfAQabPGkZiUv3i', '22222222', 'marwaIt'),
(4, 'marwa.saieb@esprit.tn', '2023-02-03 18:52:08', b'1', '$2a$10$C13OcNwiJ3ESBJDuohuM7OhT8r0iD4vFxjAzJLSKUD7QvRi2rFOE.', '455646464646', 'WoCapge'),
(11, 'Wdemo@capgemini.com', '2023-02-03 20:56:11', b'1', '$2a$10$KFkINm4oioSB8E3qygik4uA4Nq6gwZawo7OFz2mfAQabPGkZiUv3i', '454645656', 'marwaWorker'),
(12, 'Wdemo333@capgemini.com', '2023-02-03 20:56:45', b'1', '$2a$10$Oao5yqXePc2JUMMYgItkAuEgHQOu9p541uE/FurY/2/pC7ju350.2', '45575757575', 'WdWdemo3'),
(13, 'marwaHr@capgemini.com', '2023-05-08 14:58:54', b'1', '$2a$10$KFkINm4oioSB8E3qygik4uA4Nq6gwZawo7OFz2mfAQabPGkZiUv3i', '56987441', 'MarwaHr');

-- --------------------------------------------------------

--
-- Structure de la table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) NOT NULL,
  `roles_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `roles_id`) VALUES
(1, 1),
(2, 2),
(3, 4),
(4, 5),
(11, 5),
(12, 5),
(13, 3);

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

CREATE TABLE `ville` (
  `id` bigint(20) NOT NULL,
  `ville_name` varchar(255) DEFAULT NULL,
  `region_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `worker`
--

CREATE TABLE `worker` (
  `id` bigint(20) NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `inserted_at` datetime DEFAULT NULL,
  `is_enabled` bit(1) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `seniority_level` varchar(255) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `manager_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `worker`
--

INSERT INTO `worker` (`id`, `date_of_birth`, `first_name`, `inserted_at`, `is_enabled`, `last_name`, `phone_number`, `seniority_level`, `role_id`, `user_id`, `manager_id`, `project_id`) VALUES
(1, '1996-07-03', 'Worker', '2023-02-03 18:52:08', b'1', 'Capge', '455646464646', 'advanced', 5, 4, 1, 1),
(6, '2000-08-05', 'Worker', '2023-02-03 18:52:08', b'1', 'Capge', '455646464646', 'advanced', 5, 11, 1, 1),
(8, '2000-08-05', 'Wdemo333', '2023-02-03 20:56:45', b'1', 'Wdemo333', '45575757575', 'senior', 5, 12, 1, 1);

--
-- Index pour les tables déchargées
--
 SET FOREIGN_KEY_CHECKS = 0;
--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKd8vlurk7shm472n9yil1tyvym` (`role_id`),
  ADD KEY `FK8ahhk8vqegfrt6pd1p9i03aej` (`user_id`);

--
-- Index pour la table `confirmation_token`
--
ALTER TABLE `confirmation_token`
  ADD PRIMARY KEY (`tokenid`),
  ADD KEY `FKhjrtky9wbd6lbk7mu9tuddqgn` (`user_id`);

--
-- Index pour la table `departement`
--
ALTER TABLE `departement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKmy15debfb5ojxb2btrfwwm4y8` (`manager_id`);

--
-- Index pour la table `evenement`
--
ALTER TABLE `evenement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK5gnuy9f6effd6fvrekswpa6rf` (`hrr_id`);

--
-- Index pour la table `evenement_worker`
--
ALTER TABLE `evenement_worker`
  ADD KEY `FK7isc9ya411xh6ysfw11c5fn32` (`worker_id`),
  ADD KEY `FK1orqfe9q07t48qsjb3u4yqi81` (`events_id`);

--
-- Index pour la table `hrr`
--
ALTER TABLE `hrr`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKdemwukhmi1rr5i4roc0oietnw` (`role_id`),
  ADD KEY `FKohmgy67br998nkfk4ne4yy3t7` (`user_id`);

--
-- Index pour la table `it`
--
ALTER TABLE `it`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK1fd9vy192c8ldiox75y6ekjwi` (`role_id`),
  ADD KEY `FKmqx94iglidme0699lsb6vfl58` (`user_id`);

--
-- Index pour la table `manager`
--
ALTER TABLE `manager`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKgv2ho5rd1cqk4vnt9j7gbp9a4` (`role_id`),
  ADD KEY `FKlx8n4x9vqj3lqv8cj9ienwrv6` (`user_id`);

--
-- Index pour la table `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKenkemlhubaeb53r8qkyjaw7df` (`request_remote_id`),
  ADD KEY `FKjrncmp8352sd75pc3pdw4ym3r` (`worker_id`);

--
-- Index pour la table `pays`
--
ALTER TABLE `pays`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `planning`
--
ALTER TABLE `planning`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKlngnpxq5khjtjueidr12oonhr` (`request_remote_id`);

--
-- Index pour la table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK3ypxrvo06oogq5orufss0bxdt` (`pays_id`);

--
-- Index pour la table `request_document`
--
ALTER TABLE `request_document`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKpqfte8veu7r9p2yqkm25asuid` (`manager_id`),
  ADD KEY `FK406p646xqwoidfimjcjmi3qrw` (`worker_id`);

--
-- Index pour la table `request_remote`
--
ALTER TABLE `request_remote`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK5mq0uwqhc5t1rvbul3kbkp8rp` (`it_id`),
  ADD KEY `FK6k65joauyocqdl2lsjwp11ekp` (`manager_id`),
  ADD KEY `FKhxebi6j9bp9nu62g114gjs5bt` (`worker_id`);

--
-- Index pour la table `risques`
--
ALTER TABLE `risques`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKpsruwf2vlepxdua9ntni7ns3e` (`worker_id`);

--
-- Index pour la table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_bjxn5ii7v7ygwx39et0wawu0q` (`role`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UKsb8bbouer5wak8vyiiy4pf2bx` (`username`),
  ADD UNIQUE KEY `UKob8kqyqqgmefl0aco34akdtpe` (`email`);

--
-- Index pour la table `user_roles`
--
ALTER TABLE `user_roles`
  ADD KEY `FKj9553ass9uctjrmh0gkqsmv0d` (`roles_id`),
  ADD KEY `FK55itppkw3i07do3h7qoclqd4k` (`user_id`);

--
-- Index pour la table `ville`
--
ALTER TABLE `ville`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKrxxss6tb7mtrux2io14r3wygf` (`region_id`);

--
-- Index pour la table `worker`
--
ALTER TABLE `worker`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKrt045idbwchsrtfjwc91b7lyy` (`role_id`),
  ADD KEY `FKpv1q4f7cyw16v1gicvxk74skh` (`user_id`),
  ADD KEY `FKmyruwd6v2yxqtwsi4hjsjuw0l` (`manager_id`),
  ADD KEY `FKaljx9cf62s9g8u5bivyioukf3` (`project_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `departement`
--
ALTER TABLE `departement`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `evenement`
--
ALTER TABLE `evenement`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `hrr`
--
ALTER TABLE `hrr`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `it`
--
ALTER TABLE `it`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `manager`
--
ALTER TABLE `manager`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `material`
--
ALTER TABLE `material`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `pays`
--
ALTER TABLE `pays`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `planning`
--
ALTER TABLE `planning`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT pour la table `project`
--
ALTER TABLE `project`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `region`
--
ALTER TABLE `region`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `request_document`
--
ALTER TABLE `request_document`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `request_remote`
--
ALTER TABLE `request_remote`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `risques`
--
ALTER TABLE `risques`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `role`
--
ALTER TABLE `role`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `ville`
--
ALTER TABLE `ville`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `worker`
--
ALTER TABLE `worker`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `FK8ahhk8vqegfrt6pd1p9i03aej` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FKd8vlurk7shm472n9yil1tyvym` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

--
-- Contraintes pour la table `confirmation_token`
--
ALTER TABLE `confirmation_token`
  ADD CONSTRAINT `FKhjrtky9wbd6lbk7mu9tuddqgn` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `departement`
--
ALTER TABLE `departement`
  ADD CONSTRAINT `FKmy15debfb5ojxb2btrfwwm4y8` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`);

--
-- Contraintes pour la table `evenement`
--
ALTER TABLE `evenement`
  ADD CONSTRAINT `FK5gnuy9f6effd6fvrekswpa6rf` FOREIGN KEY (`hrr_id`) REFERENCES `hrr` (`id`);

--
-- Contraintes pour la table `evenement_worker`
--
ALTER TABLE `evenement_worker`
  ADD CONSTRAINT `FK1orqfe9q07t48qsjb3u4yqi81` FOREIGN KEY (`events_id`) REFERENCES `evenement` (`id`),
  ADD CONSTRAINT `FK7isc9ya411xh6ysfw11c5fn32` FOREIGN KEY (`worker_id`) REFERENCES `worker` (`id`);

--
-- Contraintes pour la table `hrr`
--
ALTER TABLE `hrr`
  ADD CONSTRAINT `FKdemwukhmi1rr5i4roc0oietnw` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `FKohmgy67br998nkfk4ne4yy3t7` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `it`
--
ALTER TABLE `it`
  ADD CONSTRAINT `FK1fd9vy192c8ldiox75y6ekjwi` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `FKmqx94iglidme0699lsb6vfl58` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `manager`
--
ALTER TABLE `manager`
  ADD CONSTRAINT `FKgv2ho5rd1cqk4vnt9j7gbp9a4` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `FKlx8n4x9vqj3lqv8cj9ienwrv6` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `material`
--
ALTER TABLE `material`
  ADD CONSTRAINT `FKenkemlhubaeb53r8qkyjaw7df` FOREIGN KEY (`request_remote_id`) REFERENCES `request_remote` (`id`),
  ADD CONSTRAINT `FKjrncmp8352sd75pc3pdw4ym3r` FOREIGN KEY (`worker_id`) REFERENCES `worker` (`id`);

--
-- Contraintes pour la table `planning`
--
ALTER TABLE `planning`
  ADD CONSTRAINT `FKlngnpxq5khjtjueidr12oonhr` FOREIGN KEY (`request_remote_id`) REFERENCES `request_remote` (`id`);

--
-- Contraintes pour la table `region`
--
ALTER TABLE `region`
  ADD CONSTRAINT `FK3ypxrvo06oogq5orufss0bxdt` FOREIGN KEY (`pays_id`) REFERENCES `pays` (`id`);

--
-- Contraintes pour la table `request_document`
--
ALTER TABLE `request_document`
  ADD CONSTRAINT `FK406p646xqwoidfimjcjmi3qrw` FOREIGN KEY (`worker_id`) REFERENCES `worker` (`id`),
  ADD CONSTRAINT `FKpqfte8veu7r9p2yqkm25asuid` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`);

--
-- Contraintes pour la table `request_remote`
--
ALTER TABLE `request_remote`
  ADD CONSTRAINT `FK5mq0uwqhc5t1rvbul3kbkp8rp` FOREIGN KEY (`it_id`) REFERENCES `it` (`id`),
  ADD CONSTRAINT `FK6k65joauyocqdl2lsjwp11ekp` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`),
  ADD CONSTRAINT `FKhxebi6j9bp9nu62g114gjs5bt` FOREIGN KEY (`worker_id`) REFERENCES `worker` (`id`);

--
-- Contraintes pour la table `risques`
--
ALTER TABLE `risques`
  ADD CONSTRAINT `FKpsruwf2vlepxdua9ntni7ns3e` FOREIGN KEY (`worker_id`) REFERENCES `worker` (`id`);

--
-- Contraintes pour la table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `FK55itppkw3i07do3h7qoclqd4k` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FKj9553ass9uctjrmh0gkqsmv0d` FOREIGN KEY (`roles_id`) REFERENCES `role` (`id`);

--
-- Contraintes pour la table `ville`
--
ALTER TABLE `ville`
  ADD CONSTRAINT `FKrxxss6tb7mtrux2io14r3wygf` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`);

--
-- Contraintes pour la table `worker`
--
ALTER TABLE `worker`
  ADD CONSTRAINT `FKaljx9cf62s9g8u5bivyioukf3` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  ADD CONSTRAINT `FKmyruwd6v2yxqtwsi4hjsjuw0l` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`),
  ADD CONSTRAINT `FKpv1q4f7cyw16v1gicvxk74skh` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FKrt045idbwchsrtfjwc91b7lyy` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
