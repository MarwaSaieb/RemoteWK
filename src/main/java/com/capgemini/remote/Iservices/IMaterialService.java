package com.capgemini.remote.Iservices;

import java.util.Optional;

import com.capgemini.remote.entity.Material;

public interface IMaterialService extends IService<Material>{
	  Optional<Material> findById(Long id);

}
