package com.capgemini.remote.Iservices;

import java.util.List;
import java.util.Optional;

import com.capgemini.remote.entity.RequestDocument;

public interface IRequestDocumentService extends IService<RequestDocument>{
	  Optional<RequestDocument> findById(Long id);

	List<RequestDocument> getRequestDocumentByWorkerId(Long id);

}
