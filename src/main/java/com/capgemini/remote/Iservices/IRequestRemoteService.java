package com.capgemini.remote.Iservices;

 
import java.util.Optional;

import com.capgemini.remote.entity.RequestRemote;

public interface IRequestRemoteService extends IService<RequestRemote>{
	  Optional<RequestRemote> findById(Long id);
	  long countRequestRemoteByExpired();
}
