package com.capgemini.remote.Iservices;

import java.util.Optional;

import com.capgemini.remote.entity.Departement;

public interface IDepartementService extends IService<Departement>{
	  Optional<Departement> findById(Long id);

}
