package com.capgemini.remote.Iservices;

 

import java.util.Optional;

import com.capgemini.remote.entity.Admin;
 


public interface IAdminService extends IService<Admin>{
	  Optional<Admin> findById(Long id);
	  

}
