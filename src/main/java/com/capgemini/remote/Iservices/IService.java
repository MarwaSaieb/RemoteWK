package com.capgemini.remote.Iservices;

import java.util.List;


public interface IService<T> {
	
	
	/* List Crud Basic G�n�rale */
	 List<T> retrieveAll();
	 T retrieveById(long id);
	 T add(T entity);
	 T update(T entity); 
	 void deleteById(T entity);
	 void deleteAll();


}
