package com.capgemini.remote.Iservices;

import java.util.Optional;

import com.capgemini.remote.entity.Risques;

public interface IRisquesService extends IService<Risques>{
	  Optional<Risques> findById(Long id);

}
