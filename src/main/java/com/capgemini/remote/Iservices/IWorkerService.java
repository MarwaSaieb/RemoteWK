package com.capgemini.remote.Iservices;

import java.util.Optional;

 
import com.capgemini.remote.entity.Worker;

public interface IWorkerService extends IService<Worker>{
	 Optional<Worker> findById(Long id);
}
