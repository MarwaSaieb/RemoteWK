package com.capgemini.remote.Iservices;

import org.springframework.stereotype.Component;

@Component
public interface IUtilService {

    String generateEmail(String first, String last);
    String generateUserName(String first, String last);
    String generateUserName2(String first, String last);
    String usernameGenerator(String fn, String ln);
}
