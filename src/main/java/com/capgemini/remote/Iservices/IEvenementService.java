package com.capgemini.remote.Iservices;

import java.util.Date;
import java.util.Optional;

import com.capgemini.remote.entity.Evenement;

public interface IEvenementService extends IService<Evenement>{
	  Optional<Evenement> findById(Long id);
	  long countEvenement(Date dateEvent);
}
