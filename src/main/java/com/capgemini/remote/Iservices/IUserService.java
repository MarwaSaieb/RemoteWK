package com.capgemini.remote.Iservices;

import java.util.List; 
 
import com.capgemini.remote.entity.Role;
import com.capgemini.remote.entity.User;

public interface IUserService extends IService<User> {
	
	List<User> findByRoles(List<Role> roles);
	
	long countAllUser();
}
