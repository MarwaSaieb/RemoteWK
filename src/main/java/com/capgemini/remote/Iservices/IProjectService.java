package com.capgemini.remote.Iservices;

import java.util.Optional;

import com.capgemini.remote.entity.Project;

public interface IProjectService extends IService<Project>{
	  Optional<Project> findById(Long id);
}
