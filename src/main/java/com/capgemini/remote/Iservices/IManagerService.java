package com.capgemini.remote.Iservices;

 

import java.util.Optional; 
import com.capgemini.remote.entity.Manager;
 

public interface IManagerService extends IService<Manager>{

	  Optional<Manager> findById(Long id);
	
}
