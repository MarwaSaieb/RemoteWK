package com.capgemini.remote.Iservices;

import java.util.Optional;

import com.capgemini.remote.entity.IT; 

public interface IItService extends IService<IT>{
	 Optional<IT> findById(Long id);

}
