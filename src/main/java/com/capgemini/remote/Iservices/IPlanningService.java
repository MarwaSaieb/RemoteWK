package com.capgemini.remote.Iservices;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.capgemini.remote.entity.Planning; 

public interface IPlanningService  extends IService<Planning>{
	  Optional<Planning> findById(Long id);
	  List<Planning> getPlanningByWorkerId(Long id);
	  Optional<Planning> findByrequestRemoteId(Long id);
	  void deleteByrequestRemoteId(Long id);
	  long countByDateRemote(Date dateRemote);
}
