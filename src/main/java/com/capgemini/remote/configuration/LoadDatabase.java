package com.capgemini.remote.configuration;

import com.capgemini.remote.entity.*;
import com.capgemini.remote.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner; 
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Configuration
public class LoadDatabase implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Autowired     
    AdminRepository adminRepository;
   
    @Autowired     
    ManagerRepository managerRepository;
    @Autowired     
    ItRepository itRepository;
    @Autowired
    UserRepository userRepository;
   
    @Autowired
    ConfirmationTokenRepository confirmationTokenRepository; 
    
    @Autowired
    private PasswordEncoder passwordEncoder;
  
    @Autowired
    RoleRepository roleRepository;


       

	@Override
	public void run(String... args) throws Exception {
		
		 if(roleRepository.findAll().isEmpty()) {
	           
             log.debug("Preloading " + roleRepository.save(new Role(1,ERole.ROLE_ADMIN)));
             log.debug("Preloading " + roleRepository.save(new Role(2,ERole.ROLE_MANAGER)));
             log.debug("Preloading " + roleRepository.save(new Role(3,ERole.ROLE_HRR)));
             log.debug("Preloading " + roleRepository.save(new Role(4,ERole.ROLE_IT)));
             log.debug("Preloading " + roleRepository.save(new Role(5,ERole.ROLE_WORKER)));
        
     } else  {
        log.debug("already loaded"); 
     }
		// TODO Auto-generated method stub
		 if(adminRepository.findAll().isEmpty()) {
	
	                Role adminRole = roleRepository.findByRole(ERole.ROLE_ADMIN).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
	                List<Role> roles = new ArrayList<>();
	                roles.add(adminRole);
	                Optional<User> saved = Optional.of(userRepository.save(new User("superadmin",passwordEncoder.encode("marwa"),"admin@capgemini.com",true,
	                        "11111111")));
	                saved.ifPresent(u -> {
	                    try {
	                        u.setRoles(roles);
	                        userRepository.save(u);
	                    }catch(Exception e) {
	                        e.printStackTrace();
	                    }
	                    });
 
	                log.debug("Preloading admin user" + saved);
	                Admin admin1 = adminRepository.save(new Admin("super", "admin",new Date()));
	                log.debug("Preloading admin");
	                admin1.setUser(saved.get());
	                adminRepository.save(admin1);
	                log.debug("Assign admin to user");
	                ConfirmationToken confirmationToken = new ConfirmationToken(saved.get());
	                confirmationTokenRepository.save(confirmationToken);
	                log.debug("Confirmation token created");
	          
	       
	                // TODO Auto-generated It First user
	                Role managerRole = roleRepository.findByRole(ERole.ROLE_MANAGER).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
	                List<Role> roles1 = new ArrayList<>();
	                roles1.add(managerRole);
             Optional<User> saved2 = Optional.of(userRepository.save(new User("manager",passwordEncoder.encode("manager"),"manager@capgemini.com",true,
                     "22222222")));
             saved2.ifPresent(u2 -> {
                 try {
                     u2.setRoles(roles1);
                     userRepository.save(u2);
                 }catch(Exception e) {
                     e.printStackTrace();
                 }
                 });
 
             log.debug("Preloading manager user" + saved);
             Manager manager1 = managerRepository.save(new Manager("superManager", "manager1",new Date()));
             log.debug("Preloading admin");
             manager1.setUser(saved2.get());
             managerRepository.save(manager1);
             log.debug("Assign Manager to user");
             ConfirmationToken confirmationToken3 = new ConfirmationToken(saved2.get());
             confirmationTokenRepository.save(confirmationToken3);
             log.debug("Confirmation token created");
			 

          // TODO Auto-generated It First user
             Role ilRole = roleRepository.findByRole(ERole.ROLE_IT).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
             List<Role> roles2 = new ArrayList<>();
             roles2.add(ilRole);
             Optional<User> saveds = Optional.of(userRepository.save(new User("it",passwordEncoder.encode("it123"),"it@capgemini.com",true,
                     "22222222")));
             saveds.ifPresent(u3 -> {
                 try {
                     u3.setRoles(roles2);
                     userRepository.save(u3);
                 }catch(Exception e) {
                     e.printStackTrace();
                 }
                 });
             
             log.debug("Preloading manager It" + saveds);
             IT it1 = itRepository.save(new IT("It", "IT",new Date()));
             log.debug("Preloading admin");
             it1.setUser(saveds.get());
             itRepository.save(it1);
             log.debug("Assign Manager to user");
             ConfirmationToken confirmationTokens = new ConfirmationToken(saveds.get());
             confirmationTokenRepository.save(confirmationTokens);
             log.debug("Confirmation token created");
             
		 } else  {
	          log.debug("there is at least one admin already");  
	        }
		 
      
	}
}