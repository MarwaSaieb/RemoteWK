package com.capgemini.remote.services;
 

import com.capgemini.remote.Iservices.IRoleService;
import com.capgemini.remote.entity.Role; 
import com.capgemini.remote.repository.RoleRepository;
 


import org.springframework.stereotype.Service;

import java.util.List; 

@Service
public class RoleServiceImpl implements IRoleService {

   
    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public void deleteAll() {
        roleRepository.deleteAll();
    }

    @Override
    public Role update(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public Role add(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public Role retrieveById(long id) {
       // return roleRepository.findById(id);
    	 return roleRepository.findById(id).get();
    }

    @Override
    public void deleteById(Role role) {
        roleRepository.delete(role);
    }


    @Override
    public List<Role> retrieveAll() {
        return roleRepository.findAll();
    }

 


    
}
