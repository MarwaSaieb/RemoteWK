package com.capgemini.remote.services;
  
import com.capgemini.remote.entity.Role;
import com.capgemini.remote.entity.User;
import com.capgemini.remote.repository.UserRepository;
import com.capgemini.remote.Iservices.IUserService;

import java.util.List;

import org.springframework.stereotype.Service; 

@Service
public class UserServiceImpl implements IUserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

	@Override
	public List<User> retrieveAll() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}

	@Override
	public User retrieveById(long id) {
		// TODO Auto-generated method stub
		  return userRepository.findById(id).get();
	}

	@Override
	public User add(User user) {
		// TODO Auto-generated method stub
		 return userRepository.save(user);
	}

	@Override
	public User update(User user) {
		// TODO Auto-generated method stub
		 return userRepository.save(user);
	}

	@Override
	public void deleteById(User user) {
		// TODO Auto-generated method stub
		userRepository.delete(user);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		userRepository.deleteAll();
	}

	@Override
	public List<User> findByRoles(List<Role> roles) {
		// TODO Auto-generated method stub
		return userRepository.findByRoles(roles);
	}

	@Override
	public long countAllUser() {
		// TODO Auto-generated method stub
		return userRepository.countAllUser();
	}

   
}
