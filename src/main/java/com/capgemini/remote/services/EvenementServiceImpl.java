package com.capgemini.remote.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.remote.Iservices.IEvenementService;
import com.capgemini.remote.entity.Evenement;
import com.capgemini.remote.repository.EvenementRepository; 

@Service
public class EvenementServiceImpl implements IEvenementService {

	@Autowired
	private EvenementRepository evenementRepository;
	

 
	@Override
	public List<Evenement> retrieveAll() {
		// TODO Auto-generated method stub
		 return evenementRepository.findAll();
	}

	@Override
	public Evenement retrieveById(long id) {
		// TODO Auto-generated method stub
		 return evenementRepository.findById(id).get();
	}

	@Override
	public Evenement add(Evenement evenement) {
		// TODO Auto-generated method stub
		 return evenementRepository.save(evenement);
	}

	@Override
	public Evenement update(Evenement evenement) {
		// TODO Auto-generated method stub
		 return evenementRepository.save(evenement);
	}

	@Override
	public void deleteById(Evenement evenement) {
		// TODO Auto-generated method stub
		evenementRepository.delete(evenement);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		evenementRepository.deleteAll();
	}

	@Override
	public Optional<Evenement> findById(Long id) {
		// TODO Auto-generated method stub
		  return evenementRepository.findById(id);
	}

	@Override
	public long countEvenement(Date dateEvent) {
		// TODO Auto-generated method stub
		 return evenementRepository.countEvenement(dateEvent);
	}
}
