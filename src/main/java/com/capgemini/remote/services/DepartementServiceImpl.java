package com.capgemini.remote.services;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.remote.Iservices.IDepartementService;
import com.capgemini.remote.entity.Departement;
import com.capgemini.remote.repository.DepartementRepository;

@Service
public class DepartementServiceImpl implements IDepartementService {
	
	@Autowired
	private DepartementRepository departementRepository; 
	@Override
	public List<Departement> retrieveAll() {
	 
		  return departementRepository.findAll();
	}

	@Override
	public Departement retrieveById(long id) {
		 
	 
		return departementRepository.findById(id).get();
	}

	@Override
	public Departement add(Departement departement) {
		// TODO Auto-generated method stub 
		 return departementRepository.save(departement);
	}

	@Override
	public Departement update(Departement departement) {
		// TODO Auto-generated method stub
		 return departementRepository.save(departement);
	}

	@Override
	public void deleteById(Departement departement) {
		// TODO Auto-generated method stub
		departementRepository.delete(departement);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		departementRepository.deleteAll();
	}

	@Override
	public Optional<Departement> findById(Long id) {
		// TODO Auto-generated method stub
		  return departementRepository.findById(id);
	}

}
