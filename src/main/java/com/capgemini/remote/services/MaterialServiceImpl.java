package com.capgemini.remote.services;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.remote.Iservices.IMaterialService;
import com.capgemini.remote.entity.Material; 
import com.capgemini.remote.repository.MaterialRepository;

@Service
public class MaterialServiceImpl implements IMaterialService {

	@Autowired
	private MaterialRepository materialRepository;
	

	@Override
	public List<Material> retrieveAll() {
		// TODO Auto-generated method stub
		return materialRepository.findAll();
	}

	@Override
	public Material retrieveById(long id) {
		// TODO Auto-generated method stub
		 return materialRepository.findById(id).get();
	}

	@Override
	public Material add(Material material) {
		// TODO Auto-generated method stub
		 return materialRepository.save(material);
	}

	@Override
	public Material update(Material material) {
		// TODO Auto-generated method stub
		 return materialRepository.save(material);
	}

	@Override
	public void deleteById(Material material) {
		// TODO Auto-generated method stub
		materialRepository.delete(material);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		materialRepository.deleteAll();
	}

	@Override
	public Optional<Material> findById(Long id) {
		// TODO Auto-generated method stub
		 return materialRepository.findById(id);
	}

}
