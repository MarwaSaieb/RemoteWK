package com.capgemini.remote.services;
 
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.remote.Iservices.IRequestRemoteService;
import com.capgemini.remote.entity.RequestRemote; 
import com.capgemini.remote.repository.RequestRemoteRepository;

@Service
public class RequestRemoteServiceImpl implements IRequestRemoteService {

	@Autowired
	private RequestRemoteRepository requestRemoteRepository;
	

	
	@Override
	public List<RequestRemote> retrieveAll() {
		// TODO Auto-generated method stub
		  return requestRemoteRepository.findAll();
	}

	@Override
	public RequestRemote retrieveById(long id) {
		// TODO Auto-generated method stub
		return requestRemoteRepository.findById(id).get();
	}

	@Override
	public RequestRemote add(RequestRemote requestRemote) {
		// TODO Auto-generated method stub
		return requestRemoteRepository.save(requestRemote);
	}

	@Override
	public RequestRemote update(RequestRemote requestRemote) {
		// TODO Auto-generated method stub
		return requestRemoteRepository.save(requestRemote);
	}

	@Override
	public void deleteById(RequestRemote requestRemote) {
		// TODO Auto-generated method stub
		requestRemoteRepository.delete(requestRemote);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		requestRemoteRepository.deleteAll();
	}

	@Override
	public Optional<RequestRemote> findById(Long id) {
		// TODO Auto-generated method stub
		 return requestRemoteRepository.findById(id);
	}

	@Override
	public long countRequestRemoteByExpired() {
		// TODO Auto-generated method stub
		 return requestRemoteRepository.countByExpired();
	}

}
