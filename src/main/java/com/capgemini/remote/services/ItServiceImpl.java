package com.capgemini.remote.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.remote.Iservices.IItService;
import com.capgemini.remote.entity.IT; 
import com.capgemini.remote.repository.ItRepository; 

@Service
public class ItServiceImpl implements IItService {
	
	@Autowired
	private ItRepository itRepository;
	@Override
	public List<IT> retrieveAll() {
		// TODO Auto-generated method stub
		  return itRepository.findAll();
	}

	@Override
	public IT retrieveById(long id) {
		// TODO Auto-generated method stub
		return itRepository.findById(id).get();
	}

	@Override
	public IT add(IT it) {
		// TODO Auto-generated method stub
		 return itRepository.save(it);
	}

	@Override
	public IT update(IT it) {
		// TODO Auto-generated method stub
		 return itRepository.save(it);
	}

	@Override
	public void deleteById(IT it) {
		// TODO Auto-generated method stub
		itRepository.delete(it);
	}

	@Override
	public void deleteAll() {
	 itRepository.deleteAll();
	}

	@Override
	public Optional<IT> findById(Long id) {
		// TODO Auto-generated method stub
		return itRepository.findById(id);
	}
 

}
