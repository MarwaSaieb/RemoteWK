package com.capgemini.remote.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.remote.Iservices.IRequestDocumentService;
import com.capgemini.remote.entity.RequestDocument;
import com.capgemini.remote.repository.RequestDocumentRepository;

@Service
public class RequestDocumentServiceImpl implements IRequestDocumentService {

	@Autowired
	private RequestDocumentRepository requestDocumentRepository;
	

	@Override
	public List<RequestDocument> retrieveAll() {
		// TODO Auto-generated method stub
		return requestDocumentRepository.findAll();
	}

	@Override
	public RequestDocument retrieveById(long id) {
		// TODO Auto-generated method stub
		return requestDocumentRepository.findById(id).get();
	}

	@Override
	public RequestDocument add(RequestDocument requestDocument) {
		// TODO Auto-generated method stub
		 return requestDocumentRepository.save(requestDocument);
	}

	@Override
	public RequestDocument update(RequestDocument requestDocument) {
		// TODO Auto-generated method stub
		return requestDocumentRepository.save(requestDocument);
	}

	@Override
	public void deleteById(RequestDocument requestDocument) {
		// TODO Auto-generated method stub
		requestDocumentRepository.delete(requestDocument);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		requestDocumentRepository.deleteAll();
	}

	@Override
	public Optional<RequestDocument> findById(Long id) {
		// TODO Auto-generated method stub
		 return requestDocumentRepository.findById(id);
	}

	@Override
	public List<RequestDocument> getRequestDocumentByWorkerId(Long id) {
		// TODO Auto-generated method stub
		return requestDocumentRepository.getRequestDocumentByWorkerId(id);
	}

}
