package com.capgemini.remote.services;
 
import com.capgemini.remote.Iservices.IHRRService;
import com.capgemini.remote.entity.HRR;
import com.capgemini.remote.repository.HRRRepository;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HRRServiceImpl implements IHRRService {
  
  
    private final HRRRepository hrrRepository;

    public HRRServiceImpl(HRRRepository hrrRepository) {
        this.hrrRepository = hrrRepository;
    }
	@Override
	public List<HRR> retrieveAll() {
		// TODO Auto-generated method stub
		 return hrrRepository.findAll();
	}
	@Override
	public HRR retrieveById(long id) {
		// TODO Auto-generated method stub
		 return hrrRepository.findById(id).get();
	}
	@Override
	public HRR add(HRR hr) {
		// TODO Auto-generated method stub
		 return hrrRepository.save(hr);
	}
	@Override
	public HRR update(HRR hr) {
		// TODO Auto-generated method stub
		 return hrrRepository.save(hr);
	}
	@Override
	public void deleteById(HRR hr) {
		// TODO Auto-generated method stub
		hrrRepository.delete(hr);
	}
	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		hrrRepository.deleteAll();
	}
	@Override
	public Optional<HRR> findById(Long id) {
		// TODO Auto-generated method stub
		 return hrrRepository.findById(id);
	}
	
     
}
