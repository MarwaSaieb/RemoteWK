package com.capgemini.remote.services;

import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.remote.Iservices.IProjectService;
import com.capgemini.remote.entity.Project; 
import com.capgemini.remote.repository.ProjectRepository;

@Service
public class ProjectServiceImpl implements IProjectService {

	@Autowired
	private ProjectRepository  projectRepository;
	


	@Override
	public List<Project> retrieveAll() {
		// TODO Auto-generated method stub
		  return projectRepository.findAll();
	}

	@Override
	public Project retrieveById(long id) {
		// TODO Auto-generated method stub
		return projectRepository.findById(id).get();
	}

	@Override
	public Project add(Project project) {
		// TODO Auto-generated method stub
		 return projectRepository.save(project);
	}

	@Override
	public Project update(Project project) {
		// TODO Auto-generated method stub
		 return projectRepository.save(project);
	}

	@Override
	public void deleteById(Project project) {
		// TODO Auto-generated method stub
		projectRepository.delete(project);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		projectRepository.deleteAll();
	}

	@Override
	public Optional<Project> findById(Long id) {
		// TODO Auto-generated method stub
		 return projectRepository.findById(id);
	}

}
