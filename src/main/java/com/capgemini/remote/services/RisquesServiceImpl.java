package com.capgemini.remote.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.remote.Iservices.IRisquesService;
import com.capgemini.remote.entity.Risques;
import com.capgemini.remote.repository.RisquesRepository;

@Service
public class RisquesServiceImpl implements IRisquesService {

	@Autowired
	private RisquesRepository risquesRepository;
	

	
	@Override
	public List<Risques> retrieveAll() {
		// TODO Auto-generated method stub
		 return risquesRepository.findAll();
	}

	@Override
	public Risques retrieveById(long id) {
		// TODO Auto-generated method stub
		 return risquesRepository.findById(id).get();
	}

	@Override
	public Risques add(Risques risques) {
		// TODO Auto-generated method stub
		 return risquesRepository.save(risques);
	}

	@Override
	public Risques update(Risques risques) {
		// TODO Auto-generated method stub
		 return risquesRepository.save(risques);
	}

	@Override
	public void deleteById(Risques risques) {
		// TODO Auto-generated method stub
		risquesRepository.delete(risques);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		risquesRepository.deleteAll();
	}

	@Override
	public Optional<Risques> findById(Long id) {
		// TODO Auto-generated method stub
		 return risquesRepository.findById(id);
	}

}
