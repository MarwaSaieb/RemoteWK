package com.capgemini.remote.services;

 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.remote.Iservices.IAdminService;
import com.capgemini.remote.entity.Admin;
import com.capgemini.remote.repository.AdminRepository;

import java.util.List;
import java.util.Optional; 

@Service 
public class AdminServiceImpl implements IAdminService {
	
	@Autowired
	private AdminRepository adminRepository;
	


	@Override
	public List<Admin> retrieveAll() {
		// TODO Auto-generated method stub
		   return adminRepository.findAll();
	}
	@Override
	public Admin retrieveById(long id) {
		// TODO Auto-generated method stub
	    return adminRepository.findById(id).get();
	}
	@Override
	public Admin add(Admin admin) {
		// TODO Auto-generated method stub
		 return adminRepository.save(admin);
	}
	@Override
	public Admin update(Admin admin) {
		// TODO Auto-generated method stub
		 return adminRepository.save(admin);
	}
	@Override
	public void deleteById(Admin admin) {
		// TODO Auto-generated method stub
		adminRepository.delete(admin);
	}
	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		 adminRepository.deleteAll();
	}
	@Override
	public Optional<Admin> findById(Long id) {
		// TODO Auto-generated method stub
	    return adminRepository.findById(id);
	}
   
	
}
