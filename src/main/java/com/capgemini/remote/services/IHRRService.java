package com.capgemini.remote.services;

import java.util.Optional;

import com.capgemini.remote.entity.Admin;
import com.capgemini.remote.entity.HRR;

public interface IHRRService extends IService<HRR>{
	 Optional<HRR> findById(Long id);
}
