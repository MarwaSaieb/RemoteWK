package com.capgemini.remote.services;

import com.capgemini.remote.repository.*;
import com.capgemini.remote.Iservices.IUtilService;
import org.springframework.stereotype.Service;

@Service
public class UtilServiceImpl implements IUtilService {

    private final UserRepository userRepository;

    public UtilServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public String generateEmail(String first, String last) {
        String firstName = replaceSpace(first);
        String lastName = replaceSpace(last);
        return (firstName+"."+lastName+"@capgemini.com");
    }

    @Override
    public String generateUserName(String first, String last) {
        String firstName, lastName;

        if (!first.isEmpty() || !last.isEmpty()) {
            if(first.length() >= 2 ){
                firstName = deleteSpace(first);
                firstName = firstName.substring(0, 2);
            } else {
                firstName = deleteSpace(first);
            }
            if (last.length() >= 6){
                lastName = deleteSpace(last);
                lastName = lastName.substring(0, 6);
            } else {
                lastName = deleteSpace(last);
            }
        } else {
            return "";
        }
        String F = firstName.substring(0, 1).toUpperCase() + firstName.substring(1);
        String L = lastName.substring(0, 1).toUpperCase() + lastName.substring(1);
        return F+L;
    }

    @Override
    public String generateUserName2(String first, String last) {
        String firstName, lastName;

        if (!first.isEmpty() || !last.isEmpty()) {
            if(first.length() >= 1 ) {
                firstName = deleteSpace(first);
                firstName = firstName.substring(0, 1);
            } else {
                firstName = deleteSpace(first);
            }
            if (last.length() >= 7) {
                lastName = deleteSpace(last);
                lastName = lastName.substring(0, 7);
            } else{

                lastName = deleteSpace(last);
            }
        } else {
            return "";
        }
        String F = firstName.substring(0, 1).toUpperCase() + firstName.substring(1);
        String L = lastName.substring(0, 1).toUpperCase() + lastName.substring(1);
        return F+L;
    }

    @Override
    public String usernameGenerator(String fn, String ln) {
        String username;
        if ((fn!=null) || (ln!=null)) {
            // generates a username and checks if it exists
            if (!userRepository.existsByUsername(generateUserName(fn, ln))) {
                username = generateUserName(fn, ln);
                // if it exists another generation method will be used
            } else if (!userRepository.existsByUsername(generateUserName2(fn, ln))) {
                username = generateUserName2(fn, ln);
            } else {
                int i = 0;
                do {
                    i++;
                    username = generateUserName(fn, ln) + i;
                } while (userRepository.existsByUsername(username));
            }
            return username;
        } else {
            return "";
        }
    }

    public String replaceSpace(String s){
        String st = s.replaceAll("\\s","-");
        return st;
    }

    public String deleteSpace(String s){
        String st = s.replaceAll("\\s","");
        return st;
    }


}
