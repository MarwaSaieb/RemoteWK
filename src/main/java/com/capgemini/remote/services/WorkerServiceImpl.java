package com.capgemini.remote.services;
 
 
import com.capgemini.remote.Iservices.IWorkerService;
import com.capgemini.remote.entity.Worker; 
import com.capgemini.remote.repository.WorkerRepository;


import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WorkerServiceImpl implements IWorkerService {
  
 
    private final WorkerRepository workerRepository;

    public WorkerServiceImpl(WorkerRepository workerRepository) {
        this.workerRepository = workerRepository;
    }
	@Override
	public List<Worker> retrieveAll() {
		// TODO Auto-generated method stub
	       return workerRepository.findAll();
	}
	@Override
	public Worker retrieveById(long id) {
		// TODO Auto-generated method stub
		return workerRepository.findById(id).get();
	}
	@Override
	public Worker add(Worker worker) {
		// TODO Auto-generated method stub
		 return workerRepository.save(worker);
	}
	@Override
	public Worker update(Worker worker) {
		// TODO Auto-generated method stub
		 return workerRepository.save(worker);
	}
	@Override
	public void deleteById(Worker worker) {
		// TODO Auto-generated method stub
		workerRepository.delete(worker);
	}
	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		workerRepository.deleteAll();
	}
	@Override
	public Optional<Worker> findById(Long id) {
		// TODO Auto-generated method stub
		 return workerRepository.findById(id);
	}
 
 
	
  
}
