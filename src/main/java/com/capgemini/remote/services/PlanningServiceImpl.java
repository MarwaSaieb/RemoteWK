package com.capgemini.remote.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;
 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service; 

import com.capgemini.remote.Iservices.IPlanningService;
import com.capgemini.remote.entity.Planning; 
import com.capgemini.remote.repository.PlanningRepository;

@Service

public class PlanningServiceImpl implements IPlanningService {

	@Autowired
	private PlanningRepository planningRepository;
	
	@Override
	public List<Planning> retrieveAll() {
		// TODO Auto-generated method stub
		 return planningRepository.findAll();
	}

	@Override
	public Planning retrieveById(long id) {
		// TODO Auto-generated method stub
		return planningRepository.findById(id).get();
	}

	@Override
	public Planning add(Planning planning) {
		// TODO Auto-generated method stub
		 return planningRepository.save(planning);
	}

	@Override
	public Planning update(Planning planning) {
		// TODO Auto-generated method stub
		 return planningRepository.save(planning);
	}

	@Override
	public void deleteById(Planning planning) {
		// TODO Auto-generated method stub
		planningRepository.delete(planning);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		planningRepository.deleteAll();
	}

	@Override
	public Optional<Planning> findById(Long id) {
		// TODO Auto-generated method stub
		return planningRepository.findById(id);
	}
	
	public List<Planning> getPlanningByWorkerId(Long id) {
		// TODO Auto-generated method stub
		return planningRepository.getPlanningByWorkerId(id);
	}

	@Override
	public Optional<Planning> findByrequestRemoteId(Long id) {
		// TODO Auto-generated method stub
		return planningRepository.findByrequestRemoteId(id);
	}

	@Override
	public void deleteByrequestRemoteId(Long id) {
		// TODO Auto-generated method stub
		planningRepository.deletePlanningByrequestRemoteId(id);
	}

	@Override
	public long countByDateRemote(Date dateRemote) {
		// TODO Auto-generated method stub
		return  planningRepository.countByDateRemote(dateRemote);
	}
}
