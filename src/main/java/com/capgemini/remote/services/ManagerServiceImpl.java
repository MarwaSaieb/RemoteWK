package com.capgemini.remote.services;


import com.capgemini.remote.Iservices.IManagerService;
import com.capgemini.remote.entity.Manager; 
import com.capgemini.remote.repository.ManagerRepository; 
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ManagerServiceImpl implements IManagerService {
	
	 
	 private final ManagerRepository managerRepository;

	    public ManagerServiceImpl(ManagerRepository managerRepository) {
	        this.managerRepository = managerRepository;
	    }

	@Override
	public List<Manager> retrieveAll() {
		// TODO Auto-generated method stub
		  return managerRepository.findAll();
	}
	

	
	@Override
	public Manager add(Manager manager) {
		// TODO Auto-generated method stub
		 return managerRepository.save(manager);
	}
	@Override
	public Manager update(Manager manager) {
		// TODO Auto-generated method stub
		 return managerRepository.save(manager);
	}
	@Override
	public void deleteById(Manager manager) {
		// TODO Auto-generated method stub
		 managerRepository.delete(manager);
	}
	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		 managerRepository.deleteAll();
		
	}
  
	@Override
	public Manager retrieveById(long id) {		
		// TODO Auto-generated method stub
		  return managerRepository.findById(id).get();
		 
		
	} 
	@Override
	public Optional<Manager> findById(Long id) {
		// TODO Auto-generated method stub
		   return managerRepository.findById(id);
	}






}
