package com.capgemini.remote.repository;
 
import com.capgemini.remote.entity.Manager; 

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagerRepository extends JpaRepository<Manager,Long> {
	 Optional<Manager> findByUser(long id);
	 Optional<Manager> findByUserId(long id);
}
