package com.capgemini.remote.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.capgemini.remote.entity.Evenement; 

@Repository
public interface EvenementRepository extends JpaRepository<Evenement, Long> {

	 @Query("SELECT Count(p) FROM Evenement p WHERE p.dateStart <=:dateEvent and p.dateEnd >=:dateEvent")
	 Long countEvenement( @Param("dateEvent")  Date  dateEvent);
	 
}
