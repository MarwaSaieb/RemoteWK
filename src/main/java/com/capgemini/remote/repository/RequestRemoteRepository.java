package com.capgemini.remote.repository;

import java.util.List; 

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.capgemini.remote.entity.RequestRemote; 

@Repository
public interface RequestRemoteRepository extends JpaRepository<RequestRemote, Long> {
	
	 List<RequestRemote> findRequestRemoteByWorkerId(Long id);
	 
	 @Query("SELECT Count(p) FROM RequestRemote p WHERE p.expired = 0")
	 long countByExpired();
}
