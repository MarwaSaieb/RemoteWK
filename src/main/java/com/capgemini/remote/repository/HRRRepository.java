package com.capgemini.remote.repository;

import com.capgemini.remote.entity.HRR; 

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HRRRepository extends JpaRepository<HRR,Long> {
	 Optional<HRR> findByUser(long id);
	 Optional<HRR> findByUserId(long id);
}
