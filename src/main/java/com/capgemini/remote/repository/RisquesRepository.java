package com.capgemini.remote.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.capgemini.remote.entity.Risques;

@Repository
public interface RisquesRepository extends JpaRepository<Risques, Long> {

}
