package com.capgemini.remote.repository;

import com.capgemini.remote.entity.Admin; 

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

 

@Repository
public interface AdminRepository extends JpaRepository<Admin, Long> {

	 Optional<Admin> findByUser(long id);
	 Optional<Admin> findByUserId(long id);
}
