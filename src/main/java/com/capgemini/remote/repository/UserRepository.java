package com.capgemini.remote.repository;
  
import com.capgemini.remote.entity.ERole;
import com.capgemini.remote.entity.Role;
import com.capgemini.remote.entity.User; 

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
 
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
    
    Optional<User> findWorkerById(Long id);
    Optional<User> findManagerById(Long id);
    Optional<User> findHRRById(Long id);
    Optional<User> findITById(Long id);
    Optional<User> findAdminById(Long id); 
    
   
    @Query("SELECT u FROM User u LEFT JOIN u.roles role WHERE role in :roles")
    List<User> findByRoles(@Param("roles") List<Role> roles);
    
    @Query("SELECT Count(p) FROM User p WHERE p.isEnabled = 1")
    long countAllUser();
}
