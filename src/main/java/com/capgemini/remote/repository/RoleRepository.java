package com.capgemini.remote.repository;

import com.capgemini.remote.entity.ERole;
import com.capgemini.remote.entity.Role;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
 
	Optional<Role> findByRole(ERole eRole);
	
	 @Query( "select u from Role u  where u.role in :roles" )
	 List<Role> GetAllByRole(@Param("roles") List<ERole> roles);
}
