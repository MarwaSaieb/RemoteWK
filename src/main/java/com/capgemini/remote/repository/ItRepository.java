package com.capgemini.remote.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
 
import com.capgemini.remote.entity.IT; 

@Repository
public interface ItRepository extends JpaRepository<IT, Long> {
	 Optional<IT> findByUser(long id);
	 Optional<IT> findByUserId(long id);
}
