package com.capgemini.remote.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.capgemini.remote.entity.Material;


@Repository
public interface MaterialRepository extends JpaRepository<Material, Long> {

}
