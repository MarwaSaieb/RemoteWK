package com.capgemini.remote.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.capgemini.remote.entity.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

}
