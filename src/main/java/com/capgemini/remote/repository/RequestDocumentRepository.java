package com.capgemini.remote.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.capgemini.remote.entity.Planning;
import com.capgemini.remote.entity.RequestDocument;

@Repository
public interface RequestDocumentRepository extends JpaRepository<RequestDocument, Long> {
	
	 

	List<RequestDocument> getRequestDocumentByWorkerId(Long id);
}
