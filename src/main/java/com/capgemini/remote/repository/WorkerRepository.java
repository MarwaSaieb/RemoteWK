package com.capgemini.remote.repository;
 
 
import com.capgemini.remote.entity.Worker;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

 

@Repository
public interface WorkerRepository extends JpaRepository<Worker, Long> {
	 Optional<Worker> findAdminByUser(long id);
	 Optional<Worker> findByUserId(long id);
	 
}
