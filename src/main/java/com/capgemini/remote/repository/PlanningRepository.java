package com.capgemini.remote.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.capgemini.remote.entity.Planning; 

@Repository
public interface PlanningRepository extends JpaRepository<Planning, Long> {
	 
	@Query("SELECT p FROM Planning p INNER JOIN RequestRemote r on p.requestRemote=r.id WHERE  r.worker.id = :id and r.manager IS NOT NULL and r.it IS NOT NULL ")
	List<Planning> getPlanningByWorkerId(@Param("id")  Long id);
	
	 Optional<Planning> findByrequestRemoteId( Long id);
	 long countByDateRemote(Date dateRemote);
	
	 @Query("SELECT p FROM Planning p INNER JOIN RequestRemote r on p.requestRemote=r.id WHERE  r.worker.id = :id and p.dateRemote = :dateRemote  and r.manager IS NOT NULL and r.it IS NOT NULL ")
	 Planning getPlanningByWorkerIdAndByDateRemote(@Param("id")  Long id, @Param("dateRemote")  Date  dateRemote);
	 
	 @Modifying
	 @Transactional
	 public void deletePlanningByrequestRemoteId(@Param("id") Long id);
}
