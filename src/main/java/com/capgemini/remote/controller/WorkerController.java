package com.capgemini.remote.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.capgemini.remote.Iservices.IWorkerService;
import com.capgemini.remote.entity.Worker;

@RestController
@RequestMapping("/worker")
public class WorkerController {

	@Autowired
	private IWorkerService workerService; 
	
   public WorkerController(IWorkerService workerService) {
       this.workerService = workerService;
    
   }
 
  
	@PostMapping("/addWorker")
	@ResponseBody
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR')")
	public Worker addWorker(HttpServletRequest httpServletRequest, @RequestBody Worker hrr) { 
	 return workerService.add(hrr);		 
		 
	}
	
	@GetMapping("/getAllWorker")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR')  or hasRole('ROLE_IT')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	public List<Worker> worker() {
	return workerService.retrieveAll();		
	}
 
	 @GetMapping("/getWorker/{id}")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR')  or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	 public Worker getWorkerById(@PathVariable Long id) {
		 return workerService.retrieveById(id);
	    }
	 
	  @DeleteMapping("/deleteWorker/{id}")
	  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') ")
	  @CrossOrigin(origins = "*", allowedHeaders = "*")
	    void deleteWorker(@PathVariable Long id) {
	        Optional<Worker> worker= workerService.findById(id);
	        worker.ifPresent(workerService::deleteById);
	    }
	  
	 @PutMapping("updateWorker")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR')  or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	  @CrossOrigin(origins = "*", allowedHeaders = "*")
	    public Worker updateWorker(@RequestBody Worker workerUp) {
		 return workerService.update(workerUp); 
		  
	    }
}
