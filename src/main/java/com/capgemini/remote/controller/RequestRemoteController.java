package com.capgemini.remote.controller;
 
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.remote.Iservices.IItService;
import com.capgemini.remote.Iservices.IManagerService;
import com.capgemini.remote.Iservices.IPlanningService;
import com.capgemini.remote.Iservices.IRequestRemoteService;
import com.capgemini.remote.entity.IT;
import com.capgemini.remote.entity.Manager;
import com.capgemini.remote.entity.Planning;
import com.capgemini.remote.entity.RequestRemote;
import com.capgemini.remote.entity.StatusWk;
import com.capgemini.remote.repository.RequestRemoteRepository; 

@RestController
@RequestMapping("/requestRemote")
public class RequestRemoteController {

	final
	IRequestRemoteService requestRemoteService;
	
	 @Autowired
	private RequestRemoteRepository requestRemoteRepository;
	 
	 @Autowired
	private IPlanningService planningService;
		@Autowired
		private IManagerService managerService; 
		@Autowired
		private IItService ItService;
		
			public RequestRemoteController(IRequestRemoteService requestRemoteService) {
			
				this.requestRemoteService = requestRemoteService;
			}
	 private static final Logger log = LoggerFactory.getLogger(RequestRemoteController.class);
	
 
	@PostMapping("/addrequestRemote")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
     public RequestRemote addRequestRemote(HttpServletRequest httpServletRequest, @RequestBody RequestRemote requestRemote)   { 
		int nbrday=calcul(requestRemote.getDateStart(),requestRemote.getDateEnd());
		 log.info("**********this is a nbrday "+nbrday);
		requestRemote.setNbrDay((long)nbrday);
		RequestRemote requestremote = requestRemoteService.add(requestRemote);
	
	        Calendar debut =Calendar.getInstance(TimeZone.getTimeZone("UTC"));		 
	    	Calendar fin =Calendar.getInstance(TimeZone.getTimeZone("UTC"));
	    	 SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy h:mm a", Locale.US);
	    	debut.setTime(requestRemote.getDateStart()) ;	 
	    	fin.setTime(requestRemote.getDateEnd());
	    	fin.add(Calendar.DATE, 1);
	    	 
	    	while (debut.before(fin)){	    		
	    	    int day = debut.get(Calendar.DAY_OF_WEEK);	    	   
	    	    if (day!=debut.SATURDAY && day!=debut.SUNDAY)
	    	    { 
	    	    int jour = debut.get(Calendar.DAY_OF_MONTH);
	    	    int month = debut.getInstance().get(0);
	    	    int year = debut.get(Calendar.YEAR);
	    	    	Planning _planning=new Planning(); 
	    	    	_planning.setDateCreation(new Date());
	    	    	_planning.setDateRemote(debut.getTime());
	    	    	_planning.setStatus(StatusWk.Remote);
	    	    	_planning.setDay(Integer.toString(jour));
	    	    	_planning.setMonth(Integer.toString(month));
	    	    	_planning.setYears(Integer.toString(year));
	    	    	_planning.setRequestRemote(requestremote);
	    	    	planningService.add(_planning);
	    	     
	            
	            }
	    	    
	    	    debut.add(Calendar.DAY_OF_WEEK,1); 
	    	 
	    }
	    	
	    	 
	 	 return requestremote;		 
	 
	}
	
	 
	
	@GetMapping("/getAllRequestRemote")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') ")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	public List<RequestRemote> getAllRequestRemote() {
		
	return requestRemoteService.retrieveAll();		
	}
   
	 @GetMapping("/getRequestRemote/{id}")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	 public RequestRemote getRequestRemoteById(@PathVariable Long id) {
		 return requestRemoteService.retrieveById(id);
	    }
	 
	  @DeleteMapping("/deleteRequestRemote/{id}")
	  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	  @CrossOrigin(origins = "*", allowedHeaders = "*")
	    void deleteRequestRemote(@PathVariable Long id) {
	        Optional<RequestRemote> RequestRemote = requestRemoteService.findById(id);
	        log.info("this is a info getAllRequestRemote"+RequestRemote.get().getId());
	        planningService.deleteByrequestRemoteId(RequestRemote.get().getId());	   
	        RequestRemote.ifPresent(requestRemoteService::deleteById);
	    }
	  
	 @PutMapping("updateRequestRemote")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	    public RequestRemote updateRequestRemote(@RequestBody RequestRemote requestRemoteUp) {
		 int nbrday=calcul(requestRemoteUp.getDateStart(),requestRemoteUp.getDateEnd());
		 log.info("**********this is a nbrday "+nbrday);
		 requestRemoteUp.setNbrDay((long)nbrday);
		 RequestRemote requestremote=requestRemoteService.update(requestRemoteUp); 
	       Calendar debut =Calendar.getInstance(TimeZone.getTimeZone("UTC"));		 
		    	Calendar fin =Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		    	 SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy h:mm a", Locale.US);
		    	debut.setTime(requestRemoteUp.getDateStart()) ;	 
		    	fin.setTime(requestRemoteUp.getDateEnd());
		    	fin.add(Calendar.DATE, 1);
		    	 
		    	while (debut.before(fin)){	    		
		    	    int day = debut.get(Calendar.DAY_OF_WEEK);	    	   
		    	    if (day!=debut.SATURDAY && day!=debut.SUNDAY)
		    	    { 
		    	    int jour = debut.get(Calendar.DAY_OF_MONTH);
		    	    int month = debut.getInstance().get(0);
		    	    int year = debut.get(Calendar.YEAR);
		    	    	Planning _planning=new Planning(); 
		    	    	_planning.setDateCreation(new Date());
		    	    	_planning.setDateRemote(debut.getTime());
		    	    	_planning.setStatus(StatusWk.Remote);
		    	    	_planning.setDay(Integer.toString(jour));
		    	    	_planning.setMonth(Integer.toString(month));
		    	    	_planning.setYears(Integer.toString(year));
		    	    	_planning.setRequestRemote(requestremote);
		    	    	planningService.add(_planning);
		    	     
		            
		            }
		    	    
		    	    debut.add(Calendar.DAY_OF_WEEK,1); 
		    	 
		    }
		    	
		    	 
		 	 return requestremote;	
		  
	    }
		@GetMapping("/getAllRequestRemoteForUser/{id}")
		@ResponseBody
		 @CrossOrigin(origins = "*", allowedHeaders = "*")
		public List<RequestRemote> getAllRequestRemoteForUser(@PathVariable Long id) {
			  List<RequestRemote> requestRemote = requestRemoteRepository.findRequestRemoteByWorkerId(id);
			  return requestRemote;
          	// return ResponseEntity.ok(requestRemote.get());
		//return requestRemoteService.retrieveAll();		
		}
		
		
		
		@PostMapping("/confirmRemoteManager/{idrequest}/{idmanager}")
		@ResponseBody
		@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')  or hasRole('ROLE_IT') ")
		@CrossOrigin(origins = "*", allowedHeaders = "*")
	     public boolean confirmRemoteManager(@PathVariable Long idrequest,@PathVariable Long idmanager )   { 
			Manager manager=managerService.retrieveById(idmanager);
			RequestRemote requestremote=requestRemoteService.retrieveById(idrequest);
			requestremote.setConfirmeManager(true);
			requestremote.setManager(manager);
			requestRemoteService.add(requestremote);
			return true;
		}

		@PostMapping("/confirmRemoteIt/{idrequest}/{idit}")
		@ResponseBody
		@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_IT') ")
		@CrossOrigin(origins = "*", allowedHeaders = "*")
	     public boolean confirmRemoteIt(@PathVariable Long idrequest,@PathVariable Long idit)   { 
			 IT it=ItService.retrieveById(idit);
			RequestRemote requestremote=requestRemoteService.retrieveById(idrequest);
			requestremote.setConfirmeIt(true);
			requestremote.setIt(it); 
			requestRemoteService.add(requestremote);
			 
			return true;	
		}
		
		private static boolean isWE(Date date)
		{
		    Calendar debut =Calendar.getInstance(TimeZone.getTimeZone("UTC"));	
		    debut.setTime(date) ;		 
			int jour = debut.get(Calendar.DAY_OF_WEEK) ; 
			return (jour==Calendar.SATURDAY || jour==Calendar.SUNDAY) ;
		}
		 @GetMapping("/getNbrdays/{debut}/{fin}")
		 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
		 @CrossOrigin(origins = "*", allowedHeaders = "*")
		 public int getNbrdays(@PathVariable Date debut,@PathVariable Date fin) {
			 return calcul(debut,fin);
		    }
		private static int calcul(Date debut, Date fin)
		{
			int nb = 0;
			long finl = fin.getTime() ;
			long debutl = debut.getTime() ;
			long duree = (finl - debutl) / 1000 / 60 / 60 / 24 ;
		 
			for (long i = 0; i<= duree; i++)
			{
				Date d = new Date(debut.getTime()+(24*60*60*1000)*i) ;
			 
				boolean we = isWE(d) ;
				 
				if(!(we))
				 nb += 1 ;
				 
			}
			return nb ;
		}	
		
		 @GetMapping("NbrRequestRemote")
		 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
		 @CrossOrigin(origins = "*", allowedHeaders = "*")
		 public long NbrRequestRemote() {
			// return dateRemote;
			 	return requestRemoteService.countRequestRemoteByExpired(); 	 
		 }
}
