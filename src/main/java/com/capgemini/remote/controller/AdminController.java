package com.capgemini.remote.controller;


import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.capgemini.remote.Iservices.IAdminService;
import com.capgemini.remote.Iservices.IRoleService;
import com.capgemini.remote.entity.Admin;


@RestController
@RequestMapping("/admin")
public class AdminController {
	
	final
	 IAdminService adminService;
    final
    IRoleService roleService;

    public AdminController(IAdminService adminService, IRoleService roleService) {
        this.adminService = adminService;
        this.roleService = roleService;
    }
  
	@PostMapping("/addAdmin")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Admin addAdmin(HttpServletRequest httpServletRequest, @RequestBody Admin admin) { 
	 return adminService.add(admin);		 
		 
	}
	
	@GetMapping("/getAllAdmin")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<Admin> getAllAdmin() {
	return adminService.retrieveAll();		
	}
   
	 @GetMapping("/getAdmin/{id}")
	 @PreAuthorize("hasRole('ROLE_ADMIN')")
	 public Admin getAdminById(@PathVariable Long id) {
		 return adminService.retrieveById(id);
	    }
	  
	  @DeleteMapping("/deleteAdmin/{id}")
	  @PreAuthorize("hasRole('ROLE_ADMIN')")
	    void deleteAdmin(@PathVariable Long id) {
	        Optional<Admin> admin = adminService.findById(id);
	        admin.ifPresent(adminService::deleteById);
	    }
	  
	 @PutMapping("updateAdmin")
	 @PreAuthorize("hasRole('ROLE_ADMIN')")
	    public Admin updateAdmin(@RequestBody Admin adminUp) {
		 return adminService.update(adminUp); 
		  
	    }
	   
   
}