package com.capgemini.remote.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.remote.Iservices.IEvenementService;
import com.capgemini.remote.entity.Evenement;

 

@RestController
@RequestMapping("/Evenement")
public class EvenementController {

 
	final
	IEvenementService evenementService; 
	
	public EvenementController(IEvenementService evenementService) {
		 
		this.evenementService = evenementService;
	}
	

	@PostMapping("/addEvenement")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR')")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public Evenement addEvenement(HttpServletRequest httpServletRequest, @RequestBody Evenement evenement) { 
	 return evenementService.add(evenement);		 
		 
	}
	
	@GetMapping("/getAllEvenement")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	public List<Evenement> getAllEvenement() {
	return evenementService.retrieveAll();		
	}
   
	 @GetMapping("/getEvenement/{id}")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	 public Evenement getEvenementById(@PathVariable Long id) {
		 return evenementService.retrieveById(id);
	    }
	 
	  @DeleteMapping("/deleteEvenement/{id}")
	  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') ")
	  @CrossOrigin(origins = "*", allowedHeaders = "*")
	    void deleteEvenement(@PathVariable Long id) {
	        Optional<Evenement> evenement = evenementService.findById(id);
	        evenement.ifPresent(evenementService::deleteById);
	    }
	  
	 @PutMapping("updateEvenement")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_WORKER')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	    public Evenement updateDepartement(@RequestBody Evenement evenementUp) {
		 return evenementService.update(evenementUp); 
		  
	    }	
	
	 @GetMapping("NbrEvent/{dateEvent}")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	 public long NbrRemote(@PathVariable("dateEvent") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateEvent) {
		// return dateRemote;
		 	return evenementService.countEvenement(dateEvent); 	 
	 }
}
