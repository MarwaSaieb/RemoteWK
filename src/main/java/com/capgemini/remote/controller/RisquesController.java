package com.capgemini.remote.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.remote.Iservices.IRisquesService;
import com.capgemini.remote.entity.Risques;


@RestController
@RequestMapping("/Risques")
public class RisquesController {

	final
	IRisquesService risquesService;

	public RisquesController(IRisquesService risquesService) {
		super();
		this.risquesService = risquesService;
	}
	
	 
	@PostMapping("/addRisques")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN')  and hasRole('ROLE_WORKER')")
	public Risques addRisques(HttpServletRequest httpServletRequest, @RequestBody Risques risques) { 
	 return risquesService.add(risques);		 
		 
	}
	
	@GetMapping("/getAllRisques")
	@ResponseBody
	 @PreAuthorize("hasRole('ROLE_ADMIN') and hasRole('ROLE_MANAGER') ")
	public List<Risques> getAllRisques() {
	return risquesService.retrieveAll();		
	}
   
	 @GetMapping("/getRisques/{id}")
	 @PreAuthorize("hasRole('ROLE_ADMIN') and hasRole('ROLE_MANAGER')  and hasRole('ROLE_WORKER') ")
	 public Risques getRisquesById(@PathVariable Long id) {
		 return risquesService.retrieveById(id);
	    }
	 
	  @DeleteMapping("/deleteRisques/{id}")
	  @PreAuthorize("hasRole('ROLE_ADMIN') and hasRole('ROLE_MANAGER') and hasRole('ROLE_WORKER')")
	    void deleteRisques(@PathVariable Long id) {
	        Optional<Risques> risques = risquesService.findById(id);
	        risques.ifPresent(risquesService::deleteById);
	    }
	  
	 @PutMapping("updateRisques")
	  @PreAuthorize("hasRole('ROLE_ADMIN')  and hasRole('ROLE_WORKER')")
	    public Risques updateRisques(@RequestBody Risques risquesUp) {
		 return risquesService.update(risquesUp); 
		  
	    }
}
