package com.capgemini.remote.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.remote.Iservices.IManagerService;
import com.capgemini.remote.Iservices.IUserService;
import com.capgemini.remote.Iservices.IUtilService;
import com.capgemini.remote.entity.Manager;
import com.capgemini.remote.entity.User;
import com.capgemini.remote.repository.UserRepository; 

@RestController
@RequestMapping("/manager")
public class ManagerController { 
	
	@Autowired
	private IManagerService managerService; 
	
 
	 
   public ManagerController(IManagerService managerService) {
       this.managerService = managerService;
    
   }
  
	@PostMapping("/addManager")
	@ResponseBody
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	public Manager addManager(HttpServletRequest httpServletRequest, @RequestBody Manager manager) { 
	
		return managerService.add(manager);		 
		 
	}
	
	@GetMapping("/getAllManager")
	@ResponseBody
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PreAuthorize("hasRole('ROLE_ADMIN')")	
	public List<Manager> getAllManager() {
	return managerService.retrieveAll();		
	}
  
	 @GetMapping("/getManager/{id}")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	 public Manager getManagerById(@PathVariable Long id) {
		 return managerService.retrieveById(id);
	    }
	 
	  @DeleteMapping("/deleteManager/{id}")
	  @PreAuthorize("hasRole('ROLE_ADMIN')")
	  @CrossOrigin(origins = "*", allowedHeaders = "*")
	  public boolean deleteManager(@PathVariable Long id) {
	        Optional<Manager> manager= managerService.findById(id);
	        manager.ifPresent(managerService::deleteById);
			return true;
	    }
	  
	 @PutMapping("updateManager")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	  @CrossOrigin(origins = "*", allowedHeaders = "*")
	    public Manager updateManager(@RequestBody Manager ManagerUp) {
		 return managerService.update(ManagerUp); 
		  
	    }
}
