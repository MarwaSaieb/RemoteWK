package com.capgemini.remote.controller;


import com.capgemini.remote.entity.*;
import com.capgemini.remote.entity.wrapper.WrapperUser;
import com.capgemini.remote.repository.*;
import com.capgemini.remote.security.jwt.JwtUtils;
import com.capgemini.remote.security.services.UserDetailsImpl;
import com.capgemini.remote.Iservices.*;
import com.capgemini.remote.payload.response.JwtResponse;
import com.capgemini.remote.payload.response.MessageResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {
	 @Autowired
	 private AuthenticationManager authenticationManager;
	 @Autowired
	 private UserRepository userRepository;
	 @Autowired
	 private AdminRepository adminRepository;
	 @Autowired
	 private WorkerRepository workerRepository;
	 @Autowired
	 private HRRRepository hrRepository;
	 @Autowired
	 private ItRepository itRepository;
	 @Autowired
	 private ManagerRepository managerRepository;
	 @Autowired
	 private PlanningRepository planningRepository;
	 
	 
	 @Autowired
	 private IUserService userService;

	 @Autowired
	 private IAdminService adminService;
	 @Autowired
	 private IWorkerService workerService;
	 @Autowired
	 private IManagerService managerService;
	 @Autowired
	 private IHRRService hrrService;
	 @Autowired
	 private IItService itService;

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private   ConfirmationTokenRepository confirmationTokenRepository;
    @Autowired
    private   JavaMailSender mailSender;
    
    @Autowired
    private IUtilService utilService;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@RequestBody User user) {
    	log.info("this is a info User"+user);
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles.get(0)
                ));
    }


    @PostMapping("/CreateUser")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
    public ResponseEntity<?> createUser(@RequestBody WrapperUser wrapperUser) {
    	 String username = "";
        User user = wrapperUser.getUser();       
        Admin admin = wrapperUser.getAdmin();
        IT it = wrapperUser.getIt();
        Manager manager = wrapperUser.getManager();
        HRR hrr = wrapperUser.getHrr();
        Worker worker = wrapperUser.getWorker(); 
       
       if(admin!=null){
            if (admin.getFirstName()!=null || admin.getLastName()!=null){
                username = utilService.usernameGenerator(admin.getFirstName(),
                        admin.getLastName());
            }
        } else if (it!=null){
                if (it.getFirstName()!=null || it.getLastName()!=null){
                username = utilService.usernameGenerator(it.getFirstName(),
                		it.getLastName());
            }
        } else if (manager!=null) {
        	
            if (manager.getFirstName()!=null || manager.getLastName()!=null) {
            	if(user.getUsername()!=null) {
           		 username =user.getUsername();
           		 }else {
           			 username = utilService.usernameGenerator(manager.getFirstName(),
                             manager.getLastName());
           		 }
               
               
            }
        } else if (hrr!=null) {
            if (hrr.getFirstName()!=null || hrr.getLastName()!=null) {
                username = utilService.usernameGenerator(hrr.getFirstName(),
                        hrr.getLastName());
            }
        } else if (worker!=null) {
            if (worker.getFirstName()!=null || worker.getLastName()!=null) {
                username = utilService.usernameGenerator(worker.getFirstName(),
                		worker.getLastName());
            }
        } else {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Name can't be empty"));
        }
       
       if(username.isEmpty()){
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Username can't be empty!"));
        }
       
         if(user.getEmail()==null || user.getEmail().isEmpty()){
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Email can't be empty!"));
        }
       
        if (userRepository.existsByUsername(username)) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(user.getEmail())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
        }
        

        // Create new user's account
        User user1 = new User(username,
                encoder.encode(user.getPassword()),
                user.getEmail(),
                user.isEnabled(),
                user.getPhoneNumber());
 
        List<Role> strRoles = user.getRoles();
        List<Role> roles = new ArrayList<>();
     
        if (strRoles == null) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Role is not found!"));
        }
        else {
            strRoles.forEach(
                    role -> {
                        switch (role.getRole()) {
                            case ROLE_ADMIN:
                                Role adminRole = roleRepository.findByRole(ERole.ROLE_ADMIN)
                                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                                roles.add(adminRole);
                                break;
                            case ROLE_IT:
                                Role itRole = roleRepository.findByRole(ERole.ROLE_IT)
                                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                                roles.add(itRole);
                                break;
                            case ROLE_HRR:
                                Role hrrRole = roleRepository.findByRole(ERole.ROLE_HRR)
                                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                                roles.add(hrrRole);
                                break;
                            case ROLE_MANAGER:
                                Role managerRole = roleRepository.findByRole(ERole.ROLE_MANAGER)
                                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                                roles.add(managerRole);
                                break;
                            case ROLE_WORKER:
                                Role workerRole = roleRepository.findByRole(ERole.ROLE_WORKER)
                                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                                roles.add(workerRole);
                                break;
                            default:
                                roles.clear();
                        }
                    }
            );
        }
     
        if(roles.isEmpty()){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new MessageResponse("Error : No role selected "));
        }
        else{
            //add user
            user1.setRoles(roles);
            userRepository.save(user1);
            log.info("roleeeeeeeeeeeeeeeeeeeeeeeeeeeee" +user1.getRoles().get(0).getRole());
            //add employee
            switch (user1.getRoles().get(0).getRole()) {
                case ROLE_ADMIN:
                    Admin _admin = adminService.add(new Admin(admin.getFirstName(), admin.getLastName(), admin.getDateOfBirth(), admin.getPhoneNumber(), admin.getInsertedAt(), admin.isEnabled(), admin.getRole()));
                    _admin.setUser(user1);
                    adminService.add(_admin);
                    break;
                case ROLE_IT:
                    IT _it = itService.add(new IT(it.getFirstName(), it.getLastName(),it.getDateOfBirth(), it.getPhoneNumber(), it.getInsertedAt(), it.isEnabled(), it.getRole()));
                    _it.setUser(user1);
                    itService.add(_it);
                    break;
                case ROLE_HRR:
                    HRR _hrr = hrrService.add(new HRR(hrr.getFirstName(), hrr.getLastName(),hrr.getDateOfBirth(), hrr.getPhoneNumber(), hrr.getInsertedAt(), hrr.isEnabled(), hrr.getRole()));
                    _hrr.setUser(user1);
                    hrrService.add(_hrr);
                    break;
                case ROLE_MANAGER:
                	  
                    Manager _manager = managerService.add(new Manager(manager.getFirstName(), manager.getLastName(), manager.getDateOfBirth(), manager.getPhoneNumber(), manager.getInsertedAt(), manager.isEnabled(), manager.getRole() ));
                    _manager.setUser(user1);
                    managerService.add(_manager);
                    break;
                case ROLE_WORKER:
              	  
                	Worker _worker = workerService.add(new Worker(worker.getId(),worker.getFirstName(), worker.getLastName(), worker.getDateOfBirth(), worker.getPhoneNumber(), worker.getInsertedAt(), worker.isEnabled(), worker.getRole(), worker.getSeniorityLevel(),worker.getManager() ,worker.getProject()));
                	_worker.setUser(user1);
                	workerService.add(_worker);
                    break;
            }
            //add confirmation token
            ConfirmationToken confirmationToken = new ConfirmationToken(user1);
            confirmationTokenRepository.save(confirmationToken);

            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(user1.getEmail());
            mailMessage.setSubject("Complete Registration!");
            mailMessage.setFrom("no-reply@capgemini.com");
            switch (user1.getRoles().get(0).getRole()) {
                case ROLE_ADMIN:
                    assert admin != null;
                    mailMessage.setText("Welcome "+admin.getFirstName()+". Your username is"+username+", To confirm your account, please click here : "
                            +"http://localhost:8090/remote/user/confirm-account?token="+confirmationToken.getToken());

                    break;
                case ROLE_MANAGER:
                    assert manager != null;
                    mailMessage.setText("Welcome "+manager.getFirstName()+". Your username is"+username+", To confirm your account, please click here : "
                            +"http://localhost:8090/remote/user/confirm-account?token="+confirmationToken.getToken());

                    break;
                case ROLE_HRR:
                    assert hrr != null;
                    mailMessage.setText("Welcome "+hrr.getFirstName()+". Your username is"+username+", To confirm your account, please click here : "
                            +"http://localhost:8090/remote/user/confirm-account?token="+confirmationToken.getToken());

                    break;
                case ROLE_IT:
                    assert it != null;
                    mailMessage.setText("Welcome "+it.getFirstName()+". Your username is"+username+", To confirm your account, please click here : "
                            +"http://localhost:8090/remote/user/confirm-account?token="+confirmationToken.getToken());

                    break;
              
                case ROLE_WORKER:
                    assert hrr != null;
                    mailMessage.setText("Welcome "+worker.getFirstName()+". Your username is"+username+", To confirm your account, please click here : "
                            +"http://localhost:8090/remote/user/confirm-account?token="+confirmationToken.getToken());

                    break;
            }
          //  mailSender.send(mailMessage);
            return ResponseEntity.ok(new MessageResponse("User added successfully! New employee must check email to confirm registration!"));
        } 
    }

    
 
    

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@RequestBody WrapperUser wrapperUser) {
        User user = wrapperUser.getUser();
        String username = "";
        Worker worker = wrapperUser.getWorker();
        if(worker!=null){
            if (worker.getFirstName()!=null || worker.getLastName()!=null){
                username = utilService.usernameGenerator(worker.getFirstName(),
                		worker.getLastName());
            }}
        else {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Name can't be empty"));
        }
        if(username.isEmpty()){
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Username can't be empty!"));
        }
        if(user.getEmail()==null || user.getEmail().isEmpty()){
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is not found!"));
        }
        if (userRepository.existsByUsername(username)) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }
        if (userRepository.existsByEmail(user.getEmail())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
        }
        // Create new user's account
        User user1 = new User(username,
                encoder.encode(user.getPassword()),
                user.getEmail(),
                user.isEnabled(),
                user.getPhoneNumber());

        List<Role> strRoles = user.getRoles();
        List<Role> roles = new ArrayList<>();

        if (strRoles == null) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Role is not found!"));
        }
        else {
            strRoles.forEach(
                    role -> {
                        if (role.getRole() == ERole.ROLE_WORKER) {
                            Role workerRole = roleRepository.findByRole(ERole.ROLE_WORKER)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(workerRole);
                        } else {
                            roles.clear();
                        }
                    }
            );
        }
        if(roles.isEmpty()){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new MessageResponse("Error : No role selected "));
        }else{

            //add user
            user1.setRoles(roles);
            userRepository.save(user1);
        }
            //add candidate
        if (user1.getRoles().get(0).getRole() == ERole.ROLE_WORKER) {
        	Worker _worker = workerService.add(new Worker(
        			worker.getFirstName(), worker.getLastName(), worker.getDateOfBirth()));
        	_worker.setUser(user1);
        	workerService.add(_worker);
        }
            //add confirmation token
            ConfirmationToken confirmationToken = new ConfirmationToken(user1);
            confirmationTokenRepository.save(confirmationToken);
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(user1.getEmail());
            mailMessage.setSubject("Complete Registration!");
            mailMessage.setFrom("no-reply@capgemini.com");
            mailMessage.setText("Hello, "+worker.getFirstName()+". Your username is"+username+" To confirm your account, please click here : "
                    +"http://localhost:8181/project/api/user/confirm-account?token="+confirmationToken.getToken());
            mailSender.send(mailMessage);
            return ResponseEntity.ok(new MessageResponse("Candidate registered successfully! Please check your mail to confirm registration!"));
    }
    
    

    @RequestMapping(value="/confirm-account", method= {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<?> confirmUserAccount(@RequestParam("token")String confirmationToken) {
        Optional<ConfirmationToken> token = confirmationTokenRepository.findByToken(confirmationToken);
        if(token.isPresent())
        {
            Optional<User> user = userRepository.findByUsername(token.get().getUser().getUsername());
            if(user.isPresent()){
                user.get().setEnabled(true);
                userRepository.save(user.get());

                return ResponseEntity.ok("<script>alert('User account has been activated successfully!');window.location = 'https://localhost:44382/';</script>");
            }
            else {
                return ResponseEntity.ok("<script>alert('Error: No user related with this link!');window.location = 'https://localhost:44382/';</script>");
            }
        }
        else
        {
            return ResponseEntity.ok("<script>alert('Error: The link is invalid or broken!');window.location = 'https://localhost:44382/';</script>");
        }
    }

  
   
    @GetMapping("/getAll")
    @ResponseBody
    public List<User> getAll() {
        return userRepository.findAll();
    }
    
    @GetMapping("/getPlanningByEmployes/{id}/{dateToday}")
    @ResponseBody
    public StatusWk getPlanningByEmployees(@PathVariable long id,@PathVariable("dateToday") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateToday)  {
    	   
    	 Planning p= planningRepository.getPlanningByWorkerIdAndByDateRemote(id, dateToday);
    	 if(p != null) {
    	 log.info("**********planning *********** "+p.getStatus());
    	 return p.getStatus();
    	 }else {
    	 return StatusWk.OnSite;
    	 }
    }
    @GetMapping("/getAllEmployes/{dateToday}")
    @ResponseBody
    public ResponseEntity<?> getAllEmployes(@PathVariable("dateToday") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateToday)  {
    	 
  
    	List<Role> roles = roleRepository.GetAllByRole(Arrays.asList(ERole.ROLE_WORKER));
    	 
    	List<User> Users=userRepository.findByRoles(roles);
    	 for (User users : Users) {
    		 Optional<User> user = userRepository.findById(users.getId()); 
    	   log.info("**********this is a info "+users.getRoles().get(0).getRole());
        	 switch (users.getRoles().get(0).getRole()) {            
                  
              case ROLE_IT:
            	  List<IT> it = itRepository.findAll();
               	 return ResponseEntity.ok(it);
                  
              case ROLE_HRR:
            	  List<HRR> hr = hrRepository.findAll();
              	 return ResponseEntity.ok(hr);
              
              case ROLE_WORKER:
            	  List<Worker> workers = workerRepository.findAll();
              
             	 return ResponseEntity.ok(workers);
                
               
          }
            return ResponseEntity.ok(users);
       
    	} ;
    	return ResponseEntity.ok(Users);
       
    }


    @DeleteMapping("/deleteUser/{id}")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<?> deleteUser(@PathVariable String id){
        Optional<User> user=userRepository.findById(Long.valueOf(id));
        if(user.isPresent()){
            if(user.get().getRoles().get(0).getRole().equals(ERole.ROLE_ADMIN)){
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Error : ADMIN could not be deleted!");
            }
            else {
                Optional<ConfirmationToken> confirmationToken=confirmationTokenRepository.findByUser(user.get());
                confirmationToken.ifPresent(confirmationTokenRepository::delete);
                userRepository.delete(user.get());
                return ResponseEntity.ok(new MessageResponse("User deleted successfully!"));
            }
        }
        else {
            return ResponseEntity.ok(new MessageResponse("Error: User is not found."));
        }
    }
    
    @PostMapping("/UpdateUser")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
    public ResponseEntity<?> updateUser(@RequestBody WrapperUser wrapperUser) {
    	 
        User user = wrapperUser.getUser();       
        Admin admin = wrapperUser.getAdmin();
        IT it = wrapperUser.getIt();
        Manager manager = wrapperUser.getManager();
        HRR hrr = wrapperUser.getHrr();
        Worker worker = wrapperUser.getWorker(); 
 
       if(user.getUsername().isEmpty()){
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Username can't be empty!"));
        }
       
         if(user.getEmail()==null || user.getEmail().isEmpty()){
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Email can't be empty!"));
        }
       
      /*  if (userRepository.existsByUsername(username)) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(user.getEmail())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
        }*/
        

        // Create new user's account
        User user1 = new User(user.getId(),user.getUsername(),
                encoder.encode(user.getPassword()),
                user.getEmail(),
                user.isEnabled(),
                user.getPhoneNumber());
 
        List<Role> strRoles = user.getRoles();
        List<Role> roles = new ArrayList<>();
     
        if (strRoles == null) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Role is not found!"));
        }
        else {
            strRoles.forEach(
                    role -> {
                        switch (role.getRole()) {
                            case ROLE_ADMIN:
                                Role adminRole = roleRepository.findByRole(ERole.ROLE_ADMIN)
                                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                                roles.add(adminRole);
                                break;
                            case ROLE_IT:
                                Role itRole = roleRepository.findByRole(ERole.ROLE_IT)
                                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                                roles.add(itRole);
                                break;
                            case ROLE_HRR:
                                Role hrrRole = roleRepository.findByRole(ERole.ROLE_HRR)
                                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                                roles.add(hrrRole);
                                break;
                            case ROLE_MANAGER:
                                Role managerRole = roleRepository.findByRole(ERole.ROLE_MANAGER)
                                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                                roles.add(managerRole);
                                break;
                            case ROLE_WORKER:
                                Role workerRole = roleRepository.findByRole(ERole.ROLE_WORKER)
                                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                                roles.add(workerRole);
                                break;
                            default:
                                roles.clear();
                        }
                    }
            );
        }
     
        if(roles.isEmpty()){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new MessageResponse("Error : No role selected "));
        }
        else{
            //add user
            user1.setRoles(roles);
            userRepository.save(user1);
            log.info("Update roleeeeeeeeeeeeeeeeeeeeeeeeeeeee" +user1.getRoles().get(0).getRole());
            //add employee
            switch (user1.getRoles().get(0).getRole()) {
                case ROLE_ADMIN:
                    Admin _admin = adminService.add(new Admin(admin.getId(),admin.getFirstName(), admin.getLastName(), admin.getDateOfBirth(), admin.getPhoneNumber(), admin.getInsertedAt(), admin.isEnabled(), admin.getRole()));
                    _admin.setUser(user1);
                    adminService.add(_admin);
                    break;
                    
                case ROLE_IT:
                	log.info("Update roleeeeeeeeeeeeeeeeeeeeeeeeeeeee2" +user1.getRoles().get(0).getRole());
                    IT _it = itService.add(new IT(it.getId(),it.getFirstName(), it.getLastName(),it.getDateOfBirth(), it.getPhoneNumber(), it.getInsertedAt(), it.isEnabled(), it.getRole()));
                    _it.setUser(user1);
                    itService.add(_it);
                    log.info("Update roleeeeeeeeeeeeeeeeeeeeeeeeeeeee3" +user1.getRoles().get(0).getRole());
                    break;
                    
                case ROLE_HRR:
                    HRR _hrr = hrrService.add(new HRR(hrr.getId(),hrr.getFirstName(), hrr.getLastName(),hrr.getDateOfBirth(), hrr.getPhoneNumber(), hrr.getInsertedAt(), hrr.isEnabled(), hrr.getRole()));
                    _hrr.setUser(user1);
                    hrrService.add(_hrr);
                    break;
                    
                case ROLE_MANAGER:
                    Manager _manager = managerService.add(new Manager(manager.getId(),manager.getFirstName(), manager.getLastName(), manager.getDateOfBirth(), manager.getPhoneNumber(), manager.getInsertedAt(), manager.isEnabled(), manager.getRole() ));
                    _manager.setUser(user1);
                    managerService.add(_manager);
                    break;
                    
                case ROLE_WORKER:                	  
                	Worker _worker = workerService.add(new Worker(worker.getId(),worker.getFirstName(), worker.getLastName(), worker.getDateOfBirth(), worker.getPhoneNumber(), worker.getInsertedAt(), worker.isEnabled(), worker.getRole(), worker.getSeniorityLevel(),worker.getManager() ,worker.getProject()));
                	_worker.setUser(user1);
                	workerService.add(_worker);
                    break;
            }
            //add confirmation token
           /* ConfirmationToken confirmationToken = new ConfirmationToken(user1);
            confirmationTokenRepository.save(confirmationToken); */
            return ResponseEntity.ok(new MessageResponse("User Update successfully! "));
        } 
    }
    
    
    
    
    
    
    
    
    @GetMapping("/{id}") 
    @ResponseBody
    public ResponseEntity<?> get(@PathVariable("id") long id) {
        Optional<User> user = userRepository.findById(id);
        log.info("**********this is a info "+user);
       if (user.isPresent()) {
    	   log.info("**********this is a info "+user.get().getRoles().get(0).getRole());
        	 switch (user.get().getRoles().get(0).getRole()) {
              case ROLE_ADMIN:
             	 Optional<Admin> admin = adminRepository.findByUserId(id);
             	 return ResponseEntity.ok(admin);
                  
              case ROLE_IT:
            	  Optional<IT> it = itRepository.findByUserId(id);
               	 return ResponseEntity.ok(it.get());
                  
              case ROLE_HRR:
            	  Optional<HRR> hr = hrRepository.findByUserId(id);
              	 return ResponseEntity.ok(hr.get());
              case ROLE_MANAGER:
            		 Optional<Manager> manager = managerRepository.findByUserId(id);
                 	 return ResponseEntity.ok(manager.get());
              case ROLE_WORKER:
             	 Optional<Worker> worker = workerRepository.findByUserId(id);
             	 return ResponseEntity.ok(worker.get());
                
               
          }
            return ResponseEntity.ok(user.get());
        } else {
            return ResponseEntity.ok(new MessageResponse("Error: No Charge Clientele found!"));
        }
    }
    
    @GetMapping("NbrAllUser")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	 public long NbrAllUser() {
		// return dateRemote;
	return userService.countAllUser(); 	 
	 }
    
    
    
}
