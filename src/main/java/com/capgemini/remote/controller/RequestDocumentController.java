package com.capgemini.remote.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.remote.Iservices.IRequestDocumentService;
import com.capgemini.remote.entity.RequestDocument;

 

@RestController
@RequestMapping("/RequestDocument")
public class RequestDocumentController {

	final
	IRequestDocumentService requestDocumentService;
	 
	public RequestDocumentController(IRequestDocumentService requestDocumentService) {
		super();
		this.requestDocumentService = requestDocumentService;
	}
	 
	@PostMapping("/addRequestDocument")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public RequestDocument addRequestDocument(HttpServletRequest httpServletRequest, @RequestBody RequestDocument requestDocument) { 
	 return requestDocumentService.add(requestDocument);		 
		 
	}
	
	@GetMapping("/getAllRequestDocument")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN') or  hasRole('ROLE_HRR') ")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public List<RequestDocument> getAllRequestDocument() {
	return requestDocumentService.retrieveAll();		
	}
	@GetMapping("/getAllRequestDocumentByWorkerId/{id}")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN') or  hasRole('ROLE_HRR') or hasRole('ROLE_WORKER') ")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public List<RequestDocument> getAllRequestDocumentByWorkerId(@PathVariable Long id) {
	return requestDocumentService.getRequestDocumentByWorkerId(id);		
	}
	 @GetMapping("/getRequestDocument/{id}")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	 public RequestDocument getRequestDocumentById(@PathVariable Long id) {
		 return requestDocumentService.retrieveById(id);
	    }
	 
	  @DeleteMapping("/deleteRequestDocument/{id}")
	  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	  @CrossOrigin(origins = "*", allowedHeaders = "*")
	    void deleteRequestDocument(@PathVariable Long id) {
	        Optional<RequestDocument> requestDocument = requestDocumentService.findById(id);
	        requestDocument.ifPresent(requestDocumentService::deleteById);
	    }
	  
	 @PutMapping("/updateRequestDocument")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	    public RequestDocument updateRequestDocument(@RequestBody RequestDocument requestDocumentUp) {
		 return requestDocumentService.update(requestDocumentUp); 
		  
	    }
	 @PutMapping("/validateRequestDocument")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	    public RequestDocument validateRequestDocument(@RequestBody RequestDocument requestDocumentUp) {
		 /*SimpleMailMessage mailMessage = new SimpleMailMessage();
         mailMessage.setTo("marwa.saieb@esprit.tn");
         mailMessage.setSubject("Complete Registration!");
         mailMessage.setFrom("no-reply@capgemini.com");
         mailMessage.setText("Hello, sss Your username is");
         mailSender.send(mailMessage);*/
		 return requestDocumentService.update(requestDocumentUp); 
		  
	    }
}
