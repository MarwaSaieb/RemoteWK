package com.capgemini.remote.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.capgemini.remote.Iservices.IDepartementService;
import com.capgemini.remote.entity.Departement;

 

@RestController
@RequestMapping("/departement")
public class DepartementController {
	final
	IDepartementService departementService;

	public DepartementController(IDepartementService departementService) {
	 
		this.departementService = departementService;
	}
	
 
	
	@PostMapping("/addDepartement")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') ")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public Departement addDepartement(HttpServletRequest httpServletRequest, @RequestBody Departement departement) { 
	 return departementService.add(departement);		 
		 
	}
	
	@GetMapping("/getAllDepartement")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') ")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public List<Departement> getAllDepartement() {
	return departementService.retrieveAll();		
	}
   
	 @GetMapping("/getDepartement/{id}")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') ")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	 public Departement getDepartementById(@PathVariable Long id) {
		 return departementService.retrieveById(id);
	    }
	 
	  @DeleteMapping("/deleteDepartement/{id}")
	  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') ")
	  @CrossOrigin(origins = "*", allowedHeaders = "*")
	    void deleteDepartement(@PathVariable Long id) {
	        Optional<Departement> departement = departementService.findById(id);
	        departement.ifPresent(departementService::deleteById);
	    }
	  
	  	@PutMapping("updateDepartement")
	  	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') ")
	  	@CrossOrigin(origins = "*", allowedHeaders = "*")
	    public Departement updateDepartement(@RequestBody Departement departementUp) {
		 return departementService.update(departementUp); 
		  
	    }
 
}
