package com.capgemini.remote.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
 
import com.capgemini.remote.Iservices.IPlanningService;
import com.capgemini.remote.entity.Planning;

@RestController
@RequestMapping("/planning")
public class PlanningController {

	final
	IPlanningService planningService;

	public PlanningController(IPlanningService planningService) {
		 
		this.planningService = planningService;
	}
 
	@PostMapping("/addPlanning")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public Planning addPlanning(HttpServletRequest httpServletRequest, @RequestBody Planning planning) { 
	 return planningService.add(planning);		 
		 
	}
	
	@GetMapping("/getAllPlanning")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public List<Planning> getAllPlanning() {
	return planningService.retrieveAll();		
	}
   
	 @GetMapping("/getPlanning/{id}")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	 public Planning getPlanningById(@PathVariable Long id) {
		 return planningService.retrieveById(id);
	    }
	 
	 @GetMapping("/getPlanningByWorker/{id}")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	 public List<Planning> getPlanningByWorker(@PathVariable Long id) {
		 return planningService.getPlanningByWorkerId(id);
	    }
	 
	  @DeleteMapping("/deletePlanning/{id}")
	  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	  @CrossOrigin(origins = "*", allowedHeaders = "*")
	    void deletePlanning(@PathVariable Long id) {
	        Optional<Planning> planning = planningService.findById(id);
	        planning.ifPresent(planningService::deleteById);
	    }
	  
	 @PutMapping("updatePlanning")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	    public Planning updatePlanning(@RequestBody Planning planningUp) {
		 return planningService.update(planningUp); 
		  
	    }
	 
	 @GetMapping("NbrRemote/{dateRemote}")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	 public long NbrRemote(@PathVariable("dateRemote") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date dateRemote) {
		// return dateRemote;
	return planningService.countByDateRemote(dateRemote); 	 
	 }
	 @DeleteMapping("/deletePlanningByRequest/{id}")
	  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER') or hasRole('ROLE_HRR') or hasRole('ROLE_IT') or hasRole('ROLE_WORKER')")
	  @CrossOrigin(origins = "*", allowedHeaders = "*")
	    void deletePlanningByRequest(@PathVariable Long id) {
	       
	        planningService.deleteByrequestRemoteId(id);
	      
	    }
}
