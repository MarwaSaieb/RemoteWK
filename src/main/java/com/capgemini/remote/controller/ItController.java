package com.capgemini.remote.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.capgemini.remote.Iservices.IItService;
import com.capgemini.remote.entity.IT;

@RestController
@RequestMapping("/It")
public class ItController {
	@Autowired
	private IItService ItService;
	
 
	@PostMapping("/addIt")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public IT addIt(HttpServletRequest httpServletRequest, @RequestBody IT it) { 
	 return ItService.add(it);		 
		 
	}
	
	@GetMapping("/getAllIt")
	@ResponseBody
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@PreAuthorize("hasRole('ROLE_ADMIN') ")
	public List<IT> getAllIt() {
	return ItService.retrieveAll();		
	}
   
	 @GetMapping("/getIt/{id}")
	 @PreAuthorize("hasRole('ROLE_ADMIN')  or hasRole('ROLE_IT')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	 public IT getAdminById(@PathVariable Long id) {
		 return ItService.retrieveById(id);
	    }
	 
	  @DeleteMapping("/deleteIt/{id}")
	  @PreAuthorize("hasRole('ROLE_ADMIN')")
	  @CrossOrigin(origins = "*", allowedHeaders = "*")
	    void deleteIt(@PathVariable Long id) {
	        Optional<IT> it = ItService.findById(id);
	        it.ifPresent(ItService::deleteById);
	    }
	  
	 @PutMapping("updateIt")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_IT')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	    public IT updateIt(@RequestBody IT itUp) {
		 return ItService.update(itUp); 
		  
	    }
	
}
