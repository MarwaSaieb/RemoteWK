package com.capgemini.remote.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.capgemini.remote.Iservices.IProjectService;
import com.capgemini.remote.entity.Project; 

@RestController
@RequestMapping("/project")
public class ProjectController {
	final
	IProjectService projectService;

	public ProjectController(IProjectService projectService) {
		
		this.projectService = projectService;
	}
	
 
	@PostMapping("/addProject")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public Project addProject(HttpServletRequest httpServletRequest, @RequestBody Project project) { 
	 return projectService.add(project);		 
		 
	}
	
	@GetMapping("/getAllProject")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public List<Project> getAllProject() {
	return projectService.retrieveAll();		
	}
   
	 @GetMapping("/getProject/{id}")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
	  @CrossOrigin(origins = "*", allowedHeaders = "*")
	 public Project getProjectById(@PathVariable Long id) {
		 return projectService.retrieveById(id);
	    }
	 
		  @DeleteMapping("/deleteProject/{id}")
		  @PreAuthorize("hasRole('ROLE_ADMIN')	or hasRole('ROLE_MANAGER')")
		  @CrossOrigin(origins = "*", allowedHeaders = "*")
	    void deleteProject(@PathVariable Long id) {
	        Optional<Project> project = projectService.findById(id);
	        project.ifPresent(projectService::deleteById);
	    }
	  
		 @PutMapping("updateProject")
		 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_MANAGER')")
		 @CrossOrigin(origins = "*", allowedHeaders = "*")
	    public Project updateProject(@RequestBody Project projectUp) {
		 return projectService.update(projectUp); 
		  
	    }
}
