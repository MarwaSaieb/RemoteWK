package com.capgemini.remote.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
 
import com.capgemini.remote.Iservices.IMaterialService;
import com.capgemini.remote.entity.Material;

@RestController
@RequestMapping("/material")
public class MaterialController {

	final
	IMaterialService materialService;

	public MaterialController(IMaterialService materialService) {
		super();
		this.materialService = materialService;
	} 
	
 
	@PostMapping("/addMaterial")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_IT') ")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public Material addMaterial(HttpServletRequest httpServletRequest, @RequestBody Material material) { 
	 return materialService.add(material);		 
		 
	}
	
	@GetMapping("/getAllMaterial")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_IT') ")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public List<Material> getAllMaterial() {
	return materialService.retrieveAll();		
	}
   
	 @GetMapping("/getMaterial/{id}")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_IT') ")
		@CrossOrigin(origins = "*", allowedHeaders = "*")
	 public Material getEvenementById(@PathVariable Long id) {
		 return materialService.retrieveById(id);
	    }
	 
	  @DeleteMapping("/deleteMaterial/{id}")
	  @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_IT') ")
		@CrossOrigin(origins = "*", allowedHeaders = "*")
	    void deleteMaterial(@PathVariable Long id) {
	    Optional<Material> material = materialService.findById(id);
	    material.ifPresent(materialService::deleteById);
	    }
	  
		@PutMapping("/updateMaterial")
		@ResponseBody
		@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_IT') ")
		@CrossOrigin(origins = "*", allowedHeaders = "*")
	    public Material updateMaterial(@RequestBody Material materialUp) {
		return materialService.update(materialUp); 
		  
	    }
}
