package com.capgemini.remote.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.remote.Iservices.IHRRService;
import com.capgemini.remote.entity.HRR;

@RestController
@RequestMapping("/hrr")
public class HRRController {

	@Autowired
	private IHRRService hrrService; 
	
   public HRRController(IHRRService hrrService) {
       this.hrrService = hrrService;
    
   }
   
  
	@PostMapping("/addHrr")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public HRR addHRR(HttpServletRequest httpServletRequest, @RequestBody HRR hrr) { 
	 return hrrService.add(hrr);		 
		 
	}
	
	@GetMapping("/getAllHRR")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@CrossOrigin(origins = "*", allowedHeaders = "*")
	public List<HRR> getAllHRR() {
	return hrrService.retrieveAll();		
	}
 
	 @GetMapping("/getHRR/{id}")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_HRR')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	 public HRR getHRRById(@PathVariable Long id) {
		 return hrrService.retrieveById(id);
	    }
	 
	  @DeleteMapping("/deleteHRR/{id}")
	  @PreAuthorize("hasRole('ROLE_ADMIN')")
	  @CrossOrigin(origins = "*", allowedHeaders = "*")
	    void deleteHRR(@PathVariable Long id) {
	        Optional<HRR> hrr= hrrService.findById(id);
	        hrr.ifPresent(hrrService::deleteById);
	    }
	  
	 @PutMapping("updateHRR")
	 @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_HRR')")
	 @CrossOrigin(origins = "*", allowedHeaders = "*")
	    public HRR updateHRR(@RequestBody HRR hrrUp) {
		 return hrrService.update(hrrUp); 
		  
	    }
}
