package com.capgemini.remote.utils;



import org.springframework.context.annotation.Configuration;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

@Configuration
public class MailUtilities {
    public static void sendmail(String recipient, String subject, String content, String filePath) throws AddressException, MessagingException, IOException {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("marwasaieb@gmail.com", "vpvpqopouskvnfav");
            }
        });
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("no-reply@worldbestbank.com", false));

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
        msg.setSubject(subject);
        msg.setContent(content, "text/html");
        msg.setSentDate(new Date());

        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent("Email sent from the world best bank", "text/html");


        if (filePath != null) {
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
            MimeBodyPart attachPart = new MimeBodyPart();
            // filePath can be like "/var/tmp/image19.png"
            attachPart.attachFile(filePath);
            multipart.addBodyPart(attachPart);
            msg.setContent(multipart);
        }

        Transport.send(msg);
    }
}
