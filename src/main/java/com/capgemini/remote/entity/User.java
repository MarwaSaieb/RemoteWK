package com.capgemini.remote.entity;

import com.fasterxml.jackson.annotation.JsonFormat; 

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

//@MappedSuperclass
@NoArgsConstructor
@ToString
@Entity
@Table(	name = "user", 
uniqueConstraints = { 
	@UniqueConstraint(columnNames = "username"),
	@UniqueConstraint(columnNames = "email") 
})
public class User implements Serializable {
    @Getter
    private static final long serialVersionUID = 3433817632185588141L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String phoneNumber;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    @Getter
    @Setter
    private Date insertedAt;

    @Getter
    @Setter
    private boolean isEnabled;

    @ManyToMany(fetch = FetchType.EAGER)
    @ToString.Exclude
    @Getter
    @Setter
    protected List<Role> roles;

    


    public User(String username, String password, String email, boolean isEnabled,  String phoneNumber) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.isEnabled = isEnabled;
        this.insertedAt = new Date();
        this.phoneNumber = phoneNumber;
    }
    public User(Long id,String username, String password, String email, boolean isEnabled,  String phoneNumber) {
    	 this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.isEnabled = isEnabled;
        this.insertedAt = new Date();
        this.phoneNumber = phoneNumber;
    }
	/*public User( String username, String password, String email, String phoneNumber, Date insertedAt,
			boolean isEnabled, List<Role> roles, Worker worker) {
		super();
		 
		this.username = username;
		this.password = password;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.insertedAt = insertedAt;
		this.isEnabled = isEnabled;
		this.roles = roles;
		this.worker = worker;
	}
    */
    
}
