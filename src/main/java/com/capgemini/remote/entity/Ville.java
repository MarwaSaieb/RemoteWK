package com.capgemini.remote.entity;

import java.io.Serializable;

import javax.persistence.*;
import lombok.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name = "ville")
public class Ville  implements Serializable {
	
	private static final long serialVersionUID = -8509683603567216064L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @Getter
    @Setter
    private Long id;

     
    @Getter
    @Setter
    protected String villeName;   
    
    
    @Getter
    @Setter
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @ToString.Exclude
    private Region region;
}
