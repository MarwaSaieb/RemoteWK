

package com.capgemini.remote.entity;
import lombok.*;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "role")
public class Role implements Serializable {
    private static final long serialVersionUID = -2930385509693609262L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @Getter
    @Setter
    private long id;

    @Column(columnDefinition = "ENUM('ROLE_ADMIN', 'ROLE_MANAGER', 'ROLE_HRR', 'ROLE_IT', 'ROLE_WORKER' )", unique = true)
    @Enumerated(EnumType.STRING)
    @Getter
    @Setter
    private ERole role;


    public Role(ERole role) {
        this.role = role;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role1 = (Role) o;
        return id == role1.id && role == role1.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, role);
    }


	
}
