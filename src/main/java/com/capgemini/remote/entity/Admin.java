package com.capgemini.remote.entity;


import javax.persistence.Entity;
import javax.persistence.Table;


import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Entity
@ToString
@NoArgsConstructor
@Table(name = "admin")
public class Admin extends Employees implements Serializable {

	public Admin(Long id, String firstName, String lastName, Date dateOfBirth, String phoneNumber, Date insertedAt,
			boolean isEnabled, Role role) {
		super(id, firstName, lastName, dateOfBirth,phoneNumber, insertedAt, isEnabled, role);
	}
	public Admin( String firstName, String lastName, Date dateOfBirth, String phoneNumber, Date insertedAt,
			boolean isEnabled, Role role) {
		super( firstName, lastName, dateOfBirth,phoneNumber, insertedAt, isEnabled, role);
	}
	public Admin(String firstName, String lastName, Date dateOfBirth) {
		super( firstName, lastName, dateOfBirth);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	 
}
