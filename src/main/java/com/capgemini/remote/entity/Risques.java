package com.capgemini.remote.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "Risques")
public class Risques implements Serializable {
	private static final long serialVersionUID = 3433817632185588141L;
		
		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name = "id", nullable = false)
	    @Getter
	    @Setter
	    private Long id;

	     
	    @Getter
	    @Setter
	    protected String typeRisque;
	    
	    @Getter
	    @Setter
	    @Column(columnDefinition="TEXT")
	    private String description;
	    
	    @Getter
	    @Setter
	    private Date dateOfRisque;
	    
	    @Getter
	    @Setter
	    private boolean status;	    
	    
	    @Getter
	    @Setter
	    @ManyToOne(optional = true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	    @ToString.Exclude
	    private Worker worker;

}
