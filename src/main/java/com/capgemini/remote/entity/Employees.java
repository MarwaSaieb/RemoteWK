package com.capgemini.remote.entity;


 
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;  
import com.fasterxml.jackson.annotation.JsonFormat;  
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@MappedSuperclass
@NoArgsConstructor
@ToString
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Employees implements Serializable {
   

	private static final long serialVersionUID = 3433817632185588141L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String firstName;

    @Getter
    @Setter
    private String lastName;

    @Getter
    @Setter
    @JsonSerialize(as = Date.class)
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd" ) 
    private Date dateOfBirth;
   
    

    @Getter
    @Setter
    private String phoneNumber;
   
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Getter
    @Setter
    private  Date  insertedAt;

    @Getter
    @Setter
    private boolean isEnabled;

    @ManyToOne
    @ToString.Exclude
    @Getter
    @Setter
    protected Role role;
 
    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    User user;
    
	public Employees(Long id, String firstName, String lastName, Date dateOfBirth,  String phoneNumber, Date insertedAt,
			boolean isEnabled, Role role) {
	 
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.phoneNumber = phoneNumber;
		this.insertedAt = new Date();
		this.isEnabled = true;
		this.role = role;
		 
	}
	public Employees( String firstName, String lastName, Date dateOfBirth,  String phoneNumber, Date insertedAt,
			boolean isEnabled, Role role) {
	 
	 
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.phoneNumber = phoneNumber;
		this.insertedAt = new Date();
		this.isEnabled = true;
		this.role = role;
		 
	}
	 public Employees( String firstName, String lastName, Date dateOfBirth) {
			super();			
			this.firstName = firstName;
			this.lastName = lastName;
			this.dateOfBirth = dateOfBirth;
			 
		}

   
    
}
