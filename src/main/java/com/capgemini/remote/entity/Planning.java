package com.capgemini.remote.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString
@NoArgsConstructor
@Table(name = "planning")
public class Planning implements Serializable {
	private static final long serialVersionUID = 1L;
	

 	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @Getter
    @Setter
    private Long id;
 	
 	    @Getter
	    @Setter
	    private Date dateCreation;
 	   @Getter
	    @Setter
	    private Date dateRemote;
 	    
 	    @Getter
	    @Setter
	    protected String day;
 	    
 	    
 	    @Getter
	    @Setter
	    protected String month;
 	    
 	    @Getter
	    @Setter
	    protected String years;
	
 	   @Enumerated(EnumType.STRING)
	    @Getter
	    @Setter
	    protected StatusWk status;
  

 	    @Getter
	    @Setter
	    @ManyToOne(optional = true, cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	    @ToString.Exclude
	    private RequestRemote requestRemote;

		public Planning(Long id, Date dateCreation, Date dateRemote, String day, String month, String years,
				StatusWk status) {
			super();
			this.id = id;
			this.dateCreation = dateCreation;
			this.dateRemote = dateRemote;
			this.day = day;
			this.month = month;
			this.years = years;
			this.status = status;
		}



		public Planning(Date dateCreation, Date dateRemote, String day, String month, String years, StatusWk status) {
			super();
			this.dateCreation = dateCreation;
			this.dateRemote = dateRemote;
			this.day = day;
			this.month = month;
			this.years = years;
			this.status = status;
		}

 



 


	 
 	    
 	    
 	    
}
