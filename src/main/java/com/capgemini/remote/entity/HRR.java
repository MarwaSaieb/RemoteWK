package com.capgemini.remote.entity;

import lombok.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "hrr")
public class HRR extends Employees implements Serializable {
  

	public HRR(Long id, String firstName, String lastName, Date dateOfBirth, String phoneNumber, Date insertedAt,
			boolean isEnabled, Role role) {
		super(id, firstName, lastName, dateOfBirth,phoneNumber, insertedAt, isEnabled, role);
		// TODO Auto-generated constructor stub
	}
	public HRR( String firstName, String lastName, Date dateOfBirth,  String phoneNumber, Date insertedAt,
			boolean isEnabled, Role role) {
		super( firstName, lastName, dateOfBirth,phoneNumber, insertedAt, isEnabled, role);
		// TODO Auto-generated constructor stub
	}
	public HRR(String firstName, String lastName, Date dateOfBirth) {
		super( firstName, lastName, dateOfBirth);
		// TODO Auto-generated constructor stub
	}
	private static final long serialVersionUID = -4365420090934109315L;

    @Getter
    @Setter
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hrr", fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<Evenement> events;
    
  

}
