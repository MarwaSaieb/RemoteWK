package com.capgemini.remote.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.*;
import javax.persistence.*;

 
@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "RequestDocument")
public class RequestDocument implements Serializable {
	private static final long serialVersionUID = 3433817632185588141L;
		
		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name = "id", nullable = false)
	    @Getter
	    @Setter
	    private Long id;

	    @Enumerated(EnumType.STRING)
	    @Getter
	    @Setter
	    protected TypeDoc typedoc;
	    
	    @Getter
	    @Setter
	    @Column(columnDefinition="TEXT")
	    private String text;
	    
	    @Getter
	    @Setter
	    private Date dateCreation;
	    
	    @Getter
	    @Setter
	    private Date dateDocument;
	    
	    @Getter
	    @Setter
	    @ManyToOne(optional = true, cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	    @ToString.Exclude
	    private Worker worker;
	    
	    @Getter
	    @Setter
	    @ManyToOne(optional = true, cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	    @ToString.Exclude
	    private Manager manager;
	    	    
	    @Getter
	    @Setter
	    private boolean status;

}
