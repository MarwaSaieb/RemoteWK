package com.capgemini.remote.entity;

public enum ESeniorityLevel {
    newbie, junior, medium, advanced, senior, expert
}
