/**
 * 
 */
package com.capgemini.remote.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType; 
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "Evenement")
public class Evenement implements Serializable {
	
	private static final long serialVersionUID = 1L;

	 	@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name = "id", nullable = false)
	    @Getter
	    @Setter
	    private Long id;

	     
	    @Getter
	    @Setter
	    protected String eventName;
	    
	    @Getter
	    @Setter
	    @Column(columnDefinition="TEXT")
	    private String description;
	    
	    @Getter
	    @Setter
	    private Date dateCreation;
	    
	 
	    @Temporal(TemporalType.TIMESTAMP)
	    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss'.000Z'")
	    @Getter
	    @Setter
	    private Date dateStart;
	    
	  
	    @Temporal(TemporalType.TIMESTAMP)
	    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss'.000Z'")
	    @Getter 
	    @Setter
	    private Date dateEnd;
	    
	    @Getter
	    @Setter
	    private boolean typeEvent;	 
	    
	    @Getter
	    @Setter
	    @ManyToOne(optional = true, cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	    @ToString.Exclude
	    private HRR hrr;
	    
	    
	    @Getter
	    @Setter
	    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	    @ToString.Exclude
	    private List<Worker> worker;
	    
}
