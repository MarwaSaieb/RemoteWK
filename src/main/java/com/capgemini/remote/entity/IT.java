package com.capgemini.remote.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.*;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "it")
public class IT extends Employees implements Serializable {
 
	public IT(Long id, String firstName, String lastName, Date dateOfBirth, String phoneNumber, Date insertedAt,
			boolean isEnabled, Role role) {
		super(id, firstName, lastName, dateOfBirth,phoneNumber, insertedAt, isEnabled, role);
		// TODO Auto-generated constructor stub
	}
	public IT( String firstName, String lastName, Date dateOfBirth, String phoneNumber, Date insertedAt,
			boolean isEnabled, Role role) {
		super( firstName, lastName, dateOfBirth,phoneNumber, insertedAt, isEnabled, role);
		// TODO Auto-generated constructor stub
	}
	public IT(String firstName, String lastName, Date dateOfBirth) {
		super( firstName, lastName, dateOfBirth);
		// TODO Auto-generated constructor stub
	}
	private static final long serialVersionUID = 1L;
	
	
		@Getter
	    @Setter
	    @JsonIgnore
	    @OneToMany(cascade = CascadeType.ALL, mappedBy = "it", fetch = FetchType.LAZY)
	    @ToString.Exclude 
	    private List<RequestRemote> requestRemote;
		
		 
}
