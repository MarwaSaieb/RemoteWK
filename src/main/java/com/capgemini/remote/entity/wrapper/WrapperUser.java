package com.capgemini.remote.entity.wrapper;

import com.capgemini.remote.entity.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


public class WrapperUser implements Serializable {

    @Getter
    private static final long serialVersionUID = -2280941126643374189L;

    @Getter
    @Setter
    User user;
    @Getter
    @Setter
    Admin admin;
    @Getter
    @Setter
    Worker worker;
    @Getter
    @Setter
    Manager manager;
    @Getter
    @Setter
    HRR hrr;
    @Getter
    @Setter
    IT it;
}
