package com.capgemini.remote.entity;

public enum TypeDoc {
	Payslip, Certificate, Contract, Bultin_de_Soin, Salary_Domiciliation, Other
}
