package com.capgemini.remote.entity;

public enum ERole {
    ROLE_ADMIN, ROLE_MANAGER, ROLE_HRR, ROLE_IT, ROLE_WORKER
}
