package com.capgemini.remote.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.*;

@Entity
@ToString
@NoArgsConstructor
@Table(name = "requestRemote")
public class RequestRemote implements Serializable {
	private static final long serialVersionUID = 1L;
	

 	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @Getter
    @Setter
    private Long id; 
 	
    @Getter
    @Setter
    private Long nbrDay;
    
    @Getter
    @Setter
    @Column(columnDefinition="TEXT")
    private String description;
    

    @Getter
    @Setter
    private Date dateStart;
    
    @Getter
    @Setter
    private Date dateEnd;
    
    @Getter
    @Setter
    private boolean ConfirmeIt;	
    
    @Getter
    @Setter
    private boolean ConfirmeManager;	
    
    
    @Getter
    @Setter
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "worker_id", referencedColumnName = "id")
    private Worker worker;
    
    
    @Getter
    @Setter
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @ToString.Exclude
    @JsonIgnore
    private Manager manager;
    
    @Getter
    @Setter
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @ToString.Exclude
    @JsonIgnore
    private IT it;
    
    @Getter
    @Setter
    private boolean expired;	
    
    @Getter
    @Setter
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "requestRemote", fetch = FetchType.LAZY)
    @ToString.Exclude 
    private List<Material> material;

}
