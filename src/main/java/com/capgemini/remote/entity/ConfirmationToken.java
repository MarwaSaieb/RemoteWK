package com.capgemini.remote.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@NoArgsConstructor
public class ConfirmationToken {
    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long tokenid;

    @Getter
    @Setter
    private String token;

    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @Getter
    @Setter
    @OneToOne(fetch = FetchType.EAGER)
    private User user;

    public ConfirmationToken(User user) {
        this.user = user;
        createdDate = new Date();
        token = UUID.randomUUID().toString();
    }
}
