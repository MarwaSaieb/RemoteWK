package com.capgemini.remote.entity;

import lombok.*;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "worker")
public class Worker extends Employees implements Serializable {
 
	private static final long serialVersionUID = 4711514648252235936L;

	@Enumerated(EnumType.STRING)
	    @Getter
	    @Setter
	    protected ESeniorityLevel seniorityLevel;

    @Getter
    @Setter
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    @ToString.Exclude
    private Manager manager;
    
    @Getter
    @Setter
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    @ToString.Exclude
    private Project project;
    
    @Getter
    @Setter
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "worker", fetch = FetchType.LAZY)
  
    private List<RequestDocument> requests;
    
    @Getter
    @Setter
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "worker", fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<Risques> risques;
    
    @Getter
    @Setter
    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "worker", fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<Evenement> events;
    
 
    
    
    @Getter
    @Setter
    @OneToMany(mappedBy = "worker", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<RequestRemote> requestRemote;
    
    @Getter
    @Setter
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "worker", fetch = FetchType.LAZY)
    @ToString.Exclude 
    private List<Material> material;

	public Worker(Long id, String firstName, String lastName, Date dateOfBirth, String phoneNumber, Date insertedAt,
			boolean isEnabled, Role role, ESeniorityLevel seniorityLevel, Manager manager, Project project) {
		super(id, firstName, lastName, dateOfBirth, phoneNumber, insertedAt, isEnabled, role);
		this.seniorityLevel = seniorityLevel;
		this.manager = manager;
		this.project = project;
	}

	public Worker(Long id, String firstName, String lastName, Date dateOfBirth, String phoneNumber, Date insertedAt,
			boolean isEnabled, Role role) {
		super(id, firstName, lastName, dateOfBirth,phoneNumber, insertedAt, isEnabled, role);
		// TODO Auto-generated constructor stub
	}
	public Worker( String firstName, String lastName, Date dateOfBirth,  String phoneNumber, Date insertedAt,
			boolean isEnabled, Role role) {
		super( firstName, lastName, dateOfBirth,phoneNumber, insertedAt, isEnabled, role);
		// TODO Auto-generated constructor stub
	}
	public Worker(String firstName, String lastName, Date dateOfBirth) {
		super( firstName, lastName, dateOfBirth);
		// TODO Auto-generated constructor stub
	}
    
}
