package com.capgemini.remote.entity;

import java.io.Serializable; 

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "Departement")
public class Departement implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @Getter
    @Setter
    private Long id;

     
    @Getter
    @Setter
    protected String DepartementName;
    
    @Getter
    @Setter
    protected Long nbrWorker;
    
    @Getter
    @Setter
    @JsonIdentityInfo(
    		  generator = ObjectIdGenerators.PropertyGenerator.class, 
    		  property = "id")
    @OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    @ToString.Exclude
    private Manager manager;
     
}
