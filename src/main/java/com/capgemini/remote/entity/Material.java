package com.capgemini.remote.entity;

import java.io.Serializable;
import javax.persistence.*;
import lombok.*;


@Entity
@ToString
@NoArgsConstructor
@Table(name = "Material")
public class Material  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	

	 	@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name = "id", nullable = false)
	    @Getter
	    @Setter
	    private Long id; 
 	
 	    @Getter
 	    @Setter
 	    protected String reference;
 	    
 	    @Getter
	    @Setter
	    protected boolean etat;
 	
 	    @Enumerated(EnumType.STRING)
	    @Getter
	    @Setter
	    protected MaterialType materialType; 	    

 	    @Getter
	    @Setter
	    @ManyToOne(optional = true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	    @ToString.Exclude
	    private RequestRemote requestRemote; 	    

 	    @Getter
	    @Setter
	    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
	    @ToString.Exclude
	    private Worker worker;

		public Material( String reference, boolean etat, MaterialType materialType, Worker worker) {
			super();
			 
			this.reference = reference;
			this.etat = etat;
			this.materialType = materialType;
			this.worker = worker;
		}
		public Material(Long id, String reference, boolean etat, MaterialType materialType, Worker worker) {
			super();
			this.id = id;
			this.reference = reference;
			this.etat = etat;
			this.materialType = materialType;
			this.worker = worker;
		}
 	    
 	    
}
