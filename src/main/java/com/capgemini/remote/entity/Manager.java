package com.capgemini.remote.entity;

import lombok.*;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "manager")
public class Manager extends Employees implements Serializable {
  

	public Manager(Long id, String firstName, String lastName, Date dateOfBirth, String phoneNumber, Date insertedAt,
			boolean isEnabled, Role role) {
		super(id, firstName, lastName, dateOfBirth,phoneNumber, insertedAt, isEnabled, role);
		// TODO Auto-generated constructor stub
	}
	public  Manager( String firstName, String lastName, Date dateOfBirth, String phoneNumber, Date insertedAt,
			boolean isEnabled, Role role) {
		super( firstName, lastName, dateOfBirth,phoneNumber, insertedAt, isEnabled, role);
		// TODO Auto-generated constructor stub
	}
	public Manager(String firstName, String lastName, Date dateOfBirth) {
		super( firstName, lastName, dateOfBirth);
		// TODO Auto-generated constructor stub
	}
	private static final long serialVersionUID = -8509683603567216064L;

    @Getter
    @Setter
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "manager", fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<Worker> teamMembers;

    @Getter
    @Setter
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "manager", fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<RequestDocument> requests;
    
    
    
    @Getter
    @Setter
    @OneToOne(cascade = CascadeType.DETACH, mappedBy = "manager", fetch = FetchType.LAZY)
    @ToString.Exclude
    @JsonIdentityInfo(
  		  generator = ObjectIdGenerators.PropertyGenerator.class, 
  		  property = "id")
    private Departement departement;
    
     
    
    @Getter
    @Setter
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "manager", fetch = FetchType.LAZY)
    @ToString.Exclude 
    private List<RequestRemote> requestRemote;
}
